# Accessibility Statement

This statement applies to content published on the domain: ‘https://data.europa.eu/’.

It does not apply to other content or websites published on any of its subdomains. These websites and their content will have their own specific accessibility statement.

This website is managed by the Publications Office of the European Union. It is designed to be used by as many people as possible, including people with disabilities.

You should be able to:

- zoom up to 200% without problems
- navigate most of the website using just a keyboard
- navigate most of the website using a modern screen reader and speech recognition software (on your computer or phone)

This website is designed to comply with the [technical standard for websites and mobile apps, EN 301 549, v.3.2.1](https://www.etsi.org/deliver/etsi_en/301500_301599/301549/03.02.01_60/en_301549v030201p.pdf). This closely follows level ‘AA’ of the [Web Content Accessibility Guidelines (WCAG) version 2.1](https://www.w3.org/TR/WCAG21/).

## Compliance status

This website is partially **compliant** with [technical standard EN 301 549 v.3.2.1](https://www.etsi.org/deliver/etsi_en/301500_301599/301549/03.02.01_60/en_301549v030201p.pdf) and the [Web Content Accessibility Guidelines (WCAG) 2.1 Level AA](https://www.w3.org/WAI/standards-guidelines/wcag/). See ‘[Non-accessible content](#non-accessible-content)’ for more details.

The website was last tested on 14 September 2023.

## Preparation of this statement

This statement was reviewed on 30/10/2023.

The statement is based on a review of a representative sample of web pages by an [IAAP](https://www.accessibilityassociation.org/s/)-qualified accessibility expert, using a combination of manual and automated testing.

## Feedback

We welcome your feedback on the accessibility of the Data.europa.eu website. Please let us know if you encounter accessibility barriers: [https://data.europa.eu/en/contact-us](https://data.europa.eu/en/contact-us)

## Compatibility with browsers and assistive technology

The Data.europa.eu website is designed to be compatible with the following most used assistive technologies:

- the latest version of Google Chrome and Apple Safari browsers;
- in combination with the latest versions of JAWS, NVDA, VoiceOver and TalkBack.

## Technical specifications

The accessibility of the Data.europa.eu website relies on the following technologies to work with the particular combination of web browser and any assistive technologies or plugins installed on your computer:

- HTML
- WAI-ARIA
- CSS
- JavaScript

## Non-accessible content

Despite our best efforts to ensure accessibility of the Data.europa.eu website, we are aware of some limitations, which we are working to fix. Below is a description of known limitations and potential solutions. Please contact us if you encounter an issue not listed below.

Known limitations for the Data.europa.eu website:

1. Some buttons and elements can not be accessed (reached) using keyboard only.
1. Some ARIA attributes are used inappropriately, which may result in not correct screen reader presentation
1. Some screen readers will not be able to read buttons or inputs correctly as they do not contain text or labels which screen readers can recognize and present.
1. The focus order of content on some pages is not always logical when navigating with a keyboard. This means that users using keyboard only for navigating the page will not be able to use it and activate functions in logical sequence.
1. Some images may not always have an appropriate alternative attribute describing its function.
1. Some links placed in the sentences may have insufficient colour contrast with the surrounding non-link text.
1. Some buttons may not have sufficient colour contrast which may make them less visible.
1. Some id attributes of the elements may be duplicated which can make it difficult to identify the component.