# How to be harvested

EU institutions, agencies and other bodies, and the Member States (the ‘data providers’) are autonomous in publishing their open data. Harvesting is the recommended method for publishers who manage their data in a data catalogue.
