# Manual publication
![type:video](../images/how-to-publish/DPIIntro.mp4)
## Access control model

The provision of datasets follows a straightforward and simple access control model. Let us assume a data provider organisation consists of multiple users (i.e. data providers), who want to manage datasets on behalf of the organisation. Each user is granted write access to one or more catalogues that belong to the data provider organisation. This access allows the user to create, update, delete, and execute any available function on any dataset in that catalogue. All users of one data provider organisation have the same view on the datasets and their state. It is up to the internal processes of a data provider organisation to manage the detailed publication process and individual responsibilities. The write access to catalogues is set by administrators of data.europa.eu.

## State of a dataset

Datasets can have two states: draft or public. A draft dataset is not publicly available via the frontend, API or SPARQL interface of the data section. It is only visible to permitted data providers. A public dataset is available like any other dataset on the data section. Datasets can be directly created as draft or public. It is possible to toggle the state of a dataset at any time.

## Registration and login

There is no self-registration to use the Data Provider Interface (DPI). Please contact [OP-DATA-EUROPA-EU@publications.europa.eu](mailto:OP-DATA-EUROPA-EU@publications.europa.eu) for requesting access to the DPI. You will be provided with access credentials (username and password).

You will find the [Data Provider Interface Login](https://data.europa.eu/euodp/en/data/user/login) in the header of the web site. The data provider interface is only available in English.
[You will find the Data Provider Interface Login in the footer under Other Service. The data provider interface is only available in English.]: #
1. Click the **Log in**  button to be taken to the login form.
[After clicking, the page redirects to the login form.]:#
![Login menu](../images/how-to-publish/loginNew1.png)

2. Enter your email address or unique identifier. If you don’t have an account yet, you can create one. If you already have an account,  click the **Next**  to proceed with logging in.
[ password and click on **Sign In**. If it is your first login process, you will be redirected to another form to change your initial password. Upon success, you will be automatically redirected to the data section.]: #
![Login page](../images/how-to-publish/sign-inNew.png)

3. Enter your password and click **Sign in** - Data Provider Interface will appear at the bottom of the web site
![Password page](../images/how-to-publish/password-form.png)

4. You can logout by simply clicking on **Logout** in the header of the web site. You will be redirected to the data section.
![Logout page](../images/how-to-publish/logout.png)