# Publication and editing via API

Alternatively, you can create, update, and delete datasets programmatically via the API of data.europa.eu. For further information on this, please refer to the [API documentation](../api-documentation/index.md) section.
