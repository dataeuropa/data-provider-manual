# How to publish on data.europa.eu

The core (meta)data model of data.europa.eu is [DCAT-AP](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe).

Essentially, DCAT-AP consists of three principal data classes: catalogues, datasets and distributions. Each data provider is represented by a catalogue. Each catalogue consists of datasets that constitute the general metadata of the data, and each dataset can have multiple distributions, where each distribution describes the actual data of the dataset in detail. All this data is serialised in the RDF format. Therefore, the DPI converts all user input into RDF. As a data provider you are concerned with the creation of datasets and distributions. The catalogues are managed by the administrators of data.europa.eu.

For more information, see the [Our metadata model](../our-metadata-model.md) section.
