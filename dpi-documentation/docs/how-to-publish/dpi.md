# Data Provider Interface (DPI) structure and function
![type:video](../videos/how-to-publish/dpi-Intro.mp4)

This section provides an overview of the structure and individual functionalities of the DPI as accessible via the DPI menu. For a more detailed demonstration, please refer to the video tutorial at the end of this manual.

![DPI menu](../images/how-to-publish/dpi-menu1.png)

The menu gives you access to high-level pages for data providers and dataset-specific functions. Some functions are context-sensitive, so they are only available on specific pages, such as a dataset details page. Functions that aren’t available are grey in the menu.

**High-level menu**

| **Function** | **Description** |
| ----- | ----- |
| **Draft Datasets** | Gives you access to the list of draft datasets of the current user. Further functions regarding the draft datasets are available on that page. |
| **My Catalogues** | Gives you access to the list of catalogues that are assigned to the current user. An assignment implies the right to create, edit and delete datasets in the respective catalogues. |
| **User Profile** | Gives you access to the profile of the current user. |
| **Logout** | Logs the current user out. |

**Dataset sub-menu**

| **Function** | **Description** |
| ----- | ----- |
| **Create Dataset** | Navigates to the form for creating a new dataset. |
| **Delete Dataset** | Deletes the current dataset. \* |
| **Edit Dataset** | Navigates to the form for editing the current dataset. \* |
| **Set to draft** | Sets the current dataset as draft, so it is not publicly visible anymore. \* |
| **Register DOI** | Registers a DOI (Digital Object Identifier) for the current dataset. \* |

\* Only available on a dataset details page.

## Create a dataset

You can create a dataset with a wizard-like form that guides you through the provision of the metadata and data. Just click on **Dataset** then **Create Dataset** in the DPI menu.

![Create a dataset](../images/how-to-publish/dpi-dataset-createNew.png)

The form is divided into five main steps: 

- define essential properties of the dataset
- define advised properties of the dataset
- define recommended properties of the dataset
- create distributions (distribution overview) 
- dataset overview

The creation of distributions is divided into several sub steps. You can always switch between the steps by clicking on the step titles or using the ‘Previous Step’ or ‘Next Step’ buttons. There might be cases when direct access to a step is not possible, for example, a mandatory field is missing.

In order to prevent accidental data loss, your input is constantly stored in the local storage of the browser.

You can clear the entire form by clicking on ‘Clear’.

You will find additional information in the i-tooltips, simply hover over them and the information will be displayed.

## Special input fields

The form consists of specialised input fields, supporting the various properties of DCAT-AP.

**Multi-lingual fields**

Some properties can be provided in multiple languages. This is supported with the following kind of input field:

![Dataset description](../images/how-to-publish/dpi-dataset-description.png)

You can just add more languages by clicking on the green button <span style="color:green">‘+ add another one‘</span> and remove them by clicking the red button the small minus sign <span style="color:red">‘- remove this property‘</span>.

**Vocabulary fields**

Many properties depend on controlled vocabularies. You can select the fitting value(s) from these vocabularies with a search-based dropdown field. Just type in some characters to find a suitable match. Below is an example for the language property.

![Dataset language](../images/how-to-publish/dpi-dataset-language.jpg)

For properties where multiple values can be selected from a vocabulary, you can easily repeat the process for each value and your selection is displayed under the input form. Please be aware that for usability reasons, the dropdown will only display a selection of ten values. If you are looking for a different value that is not displayed, simply type in the name and click it inside of the dropdown.

## Filling the form

By stepping through the wizard you are able to provide all DCAT-AP properties to describe your dataset. However, only a few properties are mandatory, such as the title and description. The 'Publish' button is not active if a mandatory property is missing.  

![Dataset validation](../images/how-to-publish/button-nopublishing.png)

Once all the mandatory fields are filled out, the 'Publish' button is activated, allowing you to publish your dataset.   

![Dataset validation](../images/how-to-publish/button-publishing.png)

## New dataset

In the following, some important details about the form are presented. However, not every property is discussed. Please consult the [DCAT-AP documentation](https://joinup.ec.europa.eu/collection/semic-support-centre/solution/dcat-application-profile-data-portals-europe) for detailed information about every property.

**Basic metadata and metadata ID**

In the first step, you provide the very basic metadata about the dataset. An important property is the dataset ID, which will be used in the URL to resolve the dataset after publication (http://data.europa.eu/88u/dataset/[dataset-id]). You can enter it yourself or it will be automatically generated based on the provided title. It can only contain lowercase letters, numbers and dashes. Its uniqueness is checked on-the-fly to avoid any clashes with existing datasets.

![Dataset metadata](../images/how-to-publish/dpi-dataset-metadataNew.png)

You must select a catalogue, which the dataset will be part of. You can only select catalogues that you have access to.

![Dataset catalog](../images/how-to-publish/dpi-dataset-catalog.png)

By clicking on ‘Next Step’ you will be directed to the second step.

**Define dataset properties**

In the second and third steps, the remaining properties can be provided.

![Dataset properties](../images/how-to-publish/dpi-dataset-properties.png)

**Create distributions**

You can provide all possible distribution data in four steps. You can repeat these steps for each distribution you want to add. To navigate through the steps, use the buttons ‘Previous Step’ and ‘Next Step’ again, or click directly on the step names.

![Dataset distribution](../images/how-to-publish/dpi-dataset-distribution.png)

A central property is the access URL, which gives you access to the actual data of the dataset. Each distribution can have several access URLs. If your data is already hosted and publicly available, you can just provide the URL by selecting the **Type** ‘Provide a URL’.

![Dataset URL type](../images/how-to-publish/dpi-dataset-url.png)

You can also upload your data directly here, by selecting the **Type** ‘Upload a file’.

![Dataset file type](../images/how-to-publish/dpi-dataset-file.png)

If you do not provide a separate download URL, the download URL is automatically set to the access URL, after saving the dataset.

On the last page of the distribution wizard you will find an overview of your created distributions. You can delete, edit them or add another distribution by clicking on **Add**

**Distribution**

![Dataset distribution overview](../images/how-to-publish/dpi-dataset-distribution-overview.png)

When you click on **Next Step** you will be redirected to the final overview of your dataset.

**Dataset overview and storing**

The final step provides you with an overview of your dataset. Note that the layout here is different from the final dataset detail page. You can still go back to previous steps and make changes to your data. If you want to finish the process, you have two options. By clicking on ‘Publish Dataset’, your dataset will be published immediately and publicly, visible to all users of the portal. You will be redirected to the public dataset details page. By clicking on ‘Save as Draft’, the dataset will be stored separately and will not be publicly available.

You will be redirected to the draft overview page. You can later edit or publish the draft dataset.

Since the access control is catalogue-based, all users that have access to the catalogue of your dataset can view and edit your draft datasets.

![Test Datensatz](../images/how-to-publish/test-datensatz.png)

## Managing datasets

You can edit, publish and delete datasets. Depending on whether the dataset is public or a draft, access to the available functions differs.

**Managing public datasets**

You can manage all datasets that are part of any catalogue you have access to. You can check and access these catalogues and their datasets by clicking on ‘My Catalogues’.

![My Catalogues](../images/how-to-publish/my-catalogues.png)

If you are on a dataset details page you use the sub-menu to access the options.

![Dataset options](../images/how-to-publish/register-doi-new.png)

‘Delete Dataset’ allows you to delete the current dataset. A final confirmation is required. ‘Edit Dataset’ will redirect you to the dataset wizard with already prefilled form fields. You can apply any changes, as if it were a new dataset. ‘Set to draft’ will un-publish the dataset and add it to the draft dataset pool.

**Managing draft datasets**

You can manage all draft datasets of your catalogues by clicking on ‘Draft Datasets’. Note that you will also see datasets here that were not created by you in person. Other users may have access to the same catalogues.

![Draft datasets](../images/how-to-publish/draft-datasets.png)

‘Delete’ allows you to delete the draft dataset. A final confirmation is required and this action cannot be undone. ‘Edit’ will redirect you to the dataset wizard with already prefilled form fields. ‘Publish’ will make the draft dataset publicly available. After that, it will not appear in the draft list anymore. The ‘Linked data’ button gives you access to the raw RDF representation in different formats.

## DOI Registration

You can easily register a DOI for your dataset. We use the registration agency of the OP to issue DOIs with the prefix 10.2906. Therefore, your dataset will be available under ‘https:// doi.org/10.2906/[id]’, where [id] is a randomly assigned number.

The registration of a DOI is permanent and should only be considered for finalised datasets. You can only register one DOI for a single dataset.

**Requirements**

Since the dataset needs to be public, a DOI can only be registered for published datasets and not for drafts. In addition to the mandatory DCAT-AP properties, you must set the following fields in your dataset: publisher, creator and the issue date and time. Without this information, the registration process will fail. 

**Register a DOI**

You can register a DOI for all datasets you have access to. Just browse to a dataset details page, open the dataset sub-menu and click on ‘Register DOI’.

![Register DOI](../images/how-to-publish/register-doi-new.png)

You will need to acknowledge the registration again.

![Confirmation](../images/how-to-publish/confirmation-new.png)

After successful registration you can reload the dataset details page and find your DOI under additional information section.

![Other identifiers](../images/how-to-publish/other-identifiers-new.png)

It is possible to repeat the process, when you have updated the metadata of the dataset, such as the title. In that case, no new id is generated, but the existing one is updated accordingly.


