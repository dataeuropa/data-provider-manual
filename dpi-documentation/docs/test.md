# LEGAL NOTICE

## Disclaimer

The Publications Office of the European Union maintains this website to enhance public access to information about its initiatives and European Union policies in general. Our goal is to keep this information timely and accurate. If errors are brought to our attention, we will try to correct them. However, the Publications Office accepts no responsibility or liability whatsoever regarding the information on this site.

This information is:

- of a general nature only and is not intended to address the specific circumstances of any individual or entity;
- not necessarily comprehensive, complete, accurate or up to date;
- sometimes linked to external sites over which the European Commission services have no control and for which the European Commission assumes no responsibility;
- not professional or legal advice (if you need specific advice, you should always consult a suitably qualified professional).

Please note that it cannot be guaranteed that a document available online exactly reproduces an officially adopted text. Only the Official Journal of the European Union (its printed edition, or since 1 July 2013 its electronic edition made available on the EUR-Lex website), is authentic and produces legal effects.

## Personal data protection

PROTECTION OF YOUR PERSONAL DATA

This privacy statement provides information about
the processing and the protection of your personal data.

 

Processing operation: Data.europa.eu 

Data Controller: Publications Office of the European Union, unit C.1, 'European Data and Related Services'

Record reference: DPR-EC-02096





 **Table of Contents**

1. Introduction
1. Why and how do we process your personal data?
1. On what legal ground(s) do we process your personal data?
1. Which personal data do we collect and further process?
1. How long do we keep your personal data?
1. How do we protect and safeguard your personal data?
1. Who has access to your personal data and to whom is it disclosed?
1. What are your rights and how can you exercise them? 
1. Contact information
1. Where to find more detailed information?

 <br>
 
     1. Introduction
     
The European Commission (hereafter ‘the Commission’) is committed to protect your personal data and to respect your privacy. The Commission collects and further processes personal data pursuant to [Regulation (EU) 2018/1725](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2018.295.01.0039.01.ENG&toc=OJ:L:2018:295:TOC) of the European Parliament and of the Council of 23 October 2018 on the protection of natural persons with regard to the processing of personal data by the Union institutions, bodies, offices and agencies and on the free movement of such data (repealing Regulation (EC) No 45/2001).

This privacy statement explains the reason for the processing of your personal data, the way we collect, handle and ensure protection of all personal data provided, how that information is used and what rights you have in relation to your personal data. It also specifies the contact details of the responsible Data Controller with whom you may exercise your rights, the Data Protection Officer and the European Data Protection Supervisor.

The information in relation to processing operation 'data.europa.eu' undertaken by unit C.1, 'European Data and Related Services' of the Publications Office of the European Union, is presented below. 


     2. Why and how do we process your personal data?

Purpose of the processing operation: Unit C.1, 'European Data and Related Services' of the Publications Office collects and uses your personal information to operate ata.europa.eu.

The following specific processing operations are carried out:


- identify authorised data providers and their operations and guarantee that only they can insert and modify their data;

- send information to data providers about the development and use of the website;

- send information and new datasets suggested by users;

- allow users to submit open data use cases via Use case forms

- send a newsletter to users who have expressed their explicit interest in receiving it (newsletter distribution list);

- carry out surveys concerning the use of the portal;

- organise and conduct Webinars (via Webex) and other training sessions on using the portal, incl. follow-up. 

- In addition, user requests received via the Helpdesk may be transferred to other Commission DGs or EU institutions (as a rule, the data provider).

Your personal data will not be used for an automated decision-making including profiling.

     3. On what legal ground(s) do we process your personal data

We process your personal data, because:

- processing is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the Union institution or body;

and

- you as data subject have given your consent to the processing of your personal data for one or more specific purposes.

Further legal bases for the processing: 


- Directive 2003/98/EC of the European Parliament and of the Council of 17 November 2003 on the re-use of public sector information (as amended by Directive 2013/37/EU), esp. Article 9
- Commission Decision 2011/833/EU on the reuse of Commission documents
- Decision 2009/496/EC, Euratom of the European Parliament, the Council, the Commission, the Court of Justice, the Court of Auditors, the European Economic and Social Committee and the Committee of the Regions of 26 June 2009 on the organisation and operation of the Publications Office of the European Union.
 

     4. Which personal data do we collect and further process?

In order to carry out this processing operation unit C.1, 'European Data and Related Services' of the Publications Office collects the following categories of personal data:

Users who are authorised data providers:


- Full name, user ID, email address, password, organisation (which provides the data); this information is needed to access personalised features like the storing of favourite datasets and favourite SPARQL queries;

If you are a participant in an online training courses and/or webinar:

- Name, email address, and date, time and duration of your participation

When you contact the Helpdesk:

- Emails may contain (apart from the email address) first name and surname, administrative address, telephone, fax and organisation. 


If you suggest a new dataset:

- Mandatory: name and email address, object and description of proposed dataset.
- Optional: country, language and type of organisation.

The provision of personal data is mandatory for the respective functionalities of Data.europa.eu.

 

     5. How long do we keep your personal data?

Unit C.1, 'European Data and Related Services' of the Publications Office only keeps your personal data for the time necessary to fulfil the purpose of collection or further processing, namely for 2 years after the last active use. 

 

     6. How do we protect and safeguard your personal data?

All personal data in electronic format (e-mails, documents, databases, uploaded batches of data, etc.) are stored either on the servers of the European Commission or of its contractors. All processing operations are carried out pursuant to the [Commission Decision (EU, Euratom) 2017/46](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1548093747090&uri=CELEX:32017D0046) of 10 January 2017 on the security of communication and information systems in the European Commission.

The Commission’s contractors are bound by a specific contractual clause for any processing operations of your data on behalf of the Commission, and by the confidentiality obligations deriving from the General Data Protection Regulation (‘GDPR’ [Regulation (EU) 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32016R0679)).

In order to protect your personal data, the Commission has put in place a number of technical and organisational measures in place. Technical measures include appropriate actions to address online security, risk of data loss, alteration of data or unauthorised access, taking into consideration the risk presented by the processing and the nature of the personal data being processed. Organisational measures include restricting access to the personal data solely to authorised persons with a legitimate need to know for the purposes of this processing operation.

 

     7. Who has access to your personal data and to whom is it disclosed?

Access to your personal data is provided to the Commission staff responsible for carrying out this processing operation and to authorised staff according to the “need to know” principle. Such staff abide by statutory, and when required, additional confidentiality agreements.
Additionally, Helpdesk requests, including the personal data contained in them, may be forwarded to staff of another Commission DG or EU institution or body.
Specific tasks are carried out by the contractor.
Consortium leader: Capgemini Invent NL, consortium partner: INTRASOFT International.

Subcontractors:

- 52 North (geospatial data technology)
- con terra (geospatial data technology)
- Agiledrop (content management systems)
- Prof. Elena Simperl (independent, King's College London) and University Politecnica de Madrid (data research and studies)
- Fraunhofer FOKUS (data technology)
- The Lisbon Council (socio-economic research and studies)
- OMMAX (SEO)
- Timelex (legal research and studies)

The portal is hosted on Amazon Cloud (Amazon Web Services, contract administered by DIGIT).

 

     8. What are your rights and how can you exercise them?

You have specific rights as a ‘data subject’ under Chapter III (Articles 14-25) of Regulation (EU) 2018/1725, in particular the right to access, rectify or erase your personal data and the right to restrict the processing of your personal data. Where applicable, you also have the right to object to the processing or the right to data portability.

You have the right to object to the processing of your personal data, which is lawfully carried out pursuant to Article 5(1)(a). 

You have consented to provide your personal data to Unit C.1, 'European Data and Related Services' of the Publications Office for the present processing operation. You can withdraw your consent at any time by notifying the Data Controller. The withdrawal will not affect the lawfulness of the processing carried out before you have withdrawn the consent.
You can exercise your rights by contacting the Data Controller, or in case of conflict the Data Protection Officer. If necessary, you can also address the European Data Protection Supervisor. Their contact information is given under Heading 9 below. 


Where you wish to exercise your rights in the context of one or several specific processing operations, please provide their description (i.e. their Record reference(s) as specified under Heading 10 below) in your request.

 

     9. Contact information

-  The Data Controller   
If you would like to exercise your rights under Regulation (EU) 2018/1725, or if you have comments, questions or concerns, or if you would like to submit a complaint regarding the collection and use of your personal data, please feel free to contact the Data Controller, Unit C.1, 'European Data and Related Services' of the Publications Office, [info@publications.europa.eu[info@publications.europa.eu].

- The Data Protection Officer (DPO) of the Commission

You may contact the [Data Protection Officer] (DATA-PROTECTION-OFFICER@ec.europa.eu) with regard to issues related to the processing of your personal data under Regulation (EU) 2018/1725.

- The European Data Protection Supervisor (EDPS)

You have the right to have recourse (i.e. you can lodge a complaint) to the European Data Protection Supervisor (edps@edps.europa.eu) if you consider that your rights under Regulation (EU) 2018/1725 have been infringed as a result of the processing of your personal data by the Data Controller.

 

    10. Where to find more detailed information?

The Commission Data Protection Officer (DPO) publishes the register of all processing operations on personal data by the Commission, which have been documented and notified to him. You may access the register via the following link: http://ec.europa.eu/dpo-register.

This specific processing operation has been included in the DPO’s public register with the following Record reference: DPR-EC-02096.

## Copyright notice

European Union, 1995–2024 


The Commission's reuse policy is implemented by [Commission Decision 2011/833/EU of 12 December 2011 on the reuse of Commission documents](https://eur-lex.europa.eu/eli/dec/2011/833/oj). 

Unless otherwise noted (e.g. in individual copyright notices), the reuse of the editorial content on this website owned by the EU is authorized under the [Creative Commons Attribution 4.0 International (CC BY 4.0) licence](https://creativecommons.org/licenses/by/4.0/). This means that reuse is allowed, provided appropriate credit is given and any changes are indicated. 

You may be required to clear additional rights if a specific content depicts identifiable private individuals or includes third-party works. To use or reproduce content that is not owned by the EU, you may need to seek permission directly from the respective rightholders. Software or documents covered by industrial property rights, such as patents, trademarks, registered designs, logos and names, are excluded from the Commission's reuse policy and are not licensed to you. 

To the extent possible under law, the European Union has waived all copyright and related or neighbouring rights to metadata of the open data portal via [Creative Commons CC0 1.0 Universal  Public Domain Dedication deed](https://creativecommons.org/publicdomain/zero/1.0/).

 

Most of the resources published display a specific reference to the licence under which the owner has chosen to release them. 

For the resources without licence information, users must consult the licence conditions in the original portal where the resources were initially published.

For all copyright issues, please contact: [(op-copyright@publications.europa.eu)].

## Personal data provided by third parties

Please note that the website contains links to third party sites and resources provided by third parties (notably via open data repositories and social media integration). Furthermore, the website publishes specific metadata relating to these resources, which are obtained directly from the information included in these resources. This metadata may contain personal data, such as the names and contact data of the persons involved in creating or managing these resources.

The website publishes such personal data based on our legitimate interest in ensuring that the information which is made publicly available by the original data source is accurately reflected in our repository, and that third parties might be able to contact the relevant persons in the way desired by the original data source. It is processed solely for the purposes of facilitating the accessibility and usability of the open data repositories which are discoverable via this website.

However, since the website does not control the third-party resources or the original personal data contained therein, we cannot vouch for its accuracy, completeness, or relevance, nor can we detect when it is confidential or has been published by mistake. Such resources are provided without assurances (explicit or implied) of their lawfulness or fitness for your purposes. We encourage any user of our website to review the relevant privacy policies of the original sources and to adhere to them, and to address any relevant data protection rights requests relating to those third-party resources to the original source, which will result in our own data being automatically corrected in time.

If addressing the matter with the original sources does not solve your issue, or if you feel that our use of the personal data is not in compliance with data protection law irrespective of the lawfulness of the original source, please contact us using the information above and we will address the concern to the best of our ability. The same procedures and rights apply as to personal data that we collect ourselves. Note however that we cannot assume responsibility for third party compliance with data protection law, and that you at any rate are and remain fully responsible for complying with data protection law for any processing activities you undertake in relation to personal data obtained via this site and via the e-services.



## Cookies

A cookie is a small text file that a website saves on your computer or mobile device when you visit a site. It enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period, so you do not have to keep re-entering them whenever you come back to the site or browse from one page to another.

To make this site work properly, the service sometimes places cookies on your device.


## How do we use cookies?
Several of our pages use cookies to remember:

your display preferences, such as contrast colour settings or font size;
- if you have already replied to a survey pop-up that asks you if the content was helpful or not (so you will not be asked again);
- if you have agreed (or not) to our use of cookies on this site.
Also, some videos embedded in our pages use a cookie to anonymously gather statistics on how you got there and what videos you visited.

Enabling these cookies is not necessary for the website to work but it will provide you with a better browsing experience. You can delete or block these cookies, but if you do that some features of this site may not work as intended.

The cookie-related information **is not used to identify you personally** and the pattern data is fully under our control. These cookies are not used for any purpose other than those described here. 


## Do we use other cookies?

Some of our pages or subsites may use additional or different cookies to the ones described above. If so, the details of these will be provided in their specific cookies notice page. You may be asked for your agreement to store these cookies.


## How to control cookies

You can **control and/or delete** cookies as you wish. You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site, and some services and functionalities may not work.