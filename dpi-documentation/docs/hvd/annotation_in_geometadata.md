High-value datasets (HVD) can also be tagged in metadata on geoinformation (ISO 19139) and be displayed as such in data.europa.eu. The geocatalogue containing this metadata will need to be harvested by data.europa.eu.
It is likely that the member state’s INSPIRE geocatalogues already include metadata on HVDs, because the [Implementing Regulation on HVDs](https://europa.eu/!PhF44v) references many data themes which are also in scope of the [INSPIRE directive](http://data.europa.eu/eli/dir/2007/2/oj).

In this case the information, that a specific dataset is a HVD, needs to be added to the metadata as a keyword.
This is done by adding a keyword “High-value dataset” and adding the European Legislation Identifier (ELI) http://data.europa.eu/eli/reg_impl/2023/138/oj.
The resulting XML will look similar to this:


```xml

<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd">
	<!—section for existing keywords -->
	<gmd:descriptiveKeywords>
		<gmd:MD_Keywords>
			<!-- HVD Keyword -->
			<gmd:keyword>
			<gmx:Anchor xlink:href="http://data.europa.eu/eli/reg_impl/2023/138/oj">High-value dataset</gmx:Anchor>
			</gmd:keyword>
		</gmd:MD_Keywords>
	</gmd:descriptiveKeywords>
```

The HVD categories are also added as keywords referencing the [Thesaurus of HVD categories](http://data.europa.eu/bna/asd487ae75) . The Open Data Directive lists the following 6 thematic categories for HVD:
- [Geospatial](http://data.europa.eu/bna/asd487ae75)
- [Earth observation and environment](http://data.europa.eu/bna/c_dd313021)
- [Meteorological](http://data.europa.eu/bna/c_b79e35eb)
- [Statistics](http://data.europa.eu/bna/c_e1da4e07)
- [Companies and company ownership](http://data.europa.eu/bna/c_a9135398)
- [Mobility](http://data.europa.eu/bna/c_b79e35eb) 

It is not necessary to provide this information in more than one language: The general language in which the metadata is provided is enough.

The resulting XML will look similar to this:

```xml

<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd">
<!-- HVD Thesaurus-Based Keywords Section -->
	<gmd:descriptiveKeywords>
		<gmd:MD_Keywords>
			<!-- Category Keyword -->
			<gmd:keyword>
				<gmx:Anchor xlink:href="http://data.europa.eu/bna/c_dd313021">Earth observation and environment</gmx:Anchor>
			</gmd:keyword>
			<!-- Thesaurus Reference -->
			<gmd:thesaurusName>
				<gmd:CI_Citation>
					<gmd:title>
						<gmx:Anchor xlink:href="http://data.europa.eu/bna/asd487ae75">High-value dataset categories</gmx:Anchor>
					</gmd:title>
					<gmd:date>
						<gmd:CI_Date>
							<gmd:date>
								<gco:Date>2023-09-05</gco:Date>
							</gmd:date>
							<gmd:dateType>
								<gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication"/>
							</gmd:dateType>
						</gmd:CI_Date>
					</gmd:date>
				</gmd:CI_Citation>
			</gmd:thesaurusName>
		</gmd:MD_Keywords>
	</gmd:descriptiveKeywords>
</gmd:MD_Metadata>
```

Optionally it is possible to add a subcategory as a keyword in the same way.

By the end of January 2025 geospatial metadata tagged in the described way will be transformed to the expected (Geo)DCAT-AP during the harvesting process.

If you are not sure whether your country’s geocatalogue is already being harvested by data.europa.eu, you can check by clicking on [catalogues](https://data.europa.eu/data/catalogues) on data.europa.eu’s start page. Then choose your country in the filter option on the left and check whether the geocatalogue is listed.
Please be aware that in some member states the national open data catalogue harvests the national INSPIRE geoportal. In this case the national open data catalogue should ensure that the geospatial HVDs are tagged accordingly.

If the geocatalogue is not yet harvested, you can [request harvesting](https://dataeuropa.gitlab.io/data-provider-manual/how-to-publish/request-harvesting/) by using the [contact form](https://data.europa.eu/en/contact-us).

[Link to example](https://gitlab.com/dataeuropa/Candidate-Iso_Hvd/-/blob/master/CANDIDATE-ISO_HVD_Tagging_Anchor_Non-Multilingual.xml)
