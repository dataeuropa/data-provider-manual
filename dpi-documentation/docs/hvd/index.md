# High-value datasets

Public sector bodies produce, collect and pay for vast amounts of data, known as public sector information or government data. Examples include geographical information, statistics and data from publicly funded research projects. Open public data refers to public sector information that can be readily and widely accessed and reused, ideally under non-restrictive conditions, accompanied by a license that allows the data to be accessed, used, and shared for commercial and non-commercial purposes.

Certain public sector data are particularly interesting for creators of value-added services and applications and have **important benefits** for society, the environment and the economy. That is why they should be made available to the public under conditions that make their reuse easier.

Further to the [Open Data Directive](https://europa.eu/!xQY9kg){:target="_blank"} that set the legal framework for open data, based on the key principles of transparency and fair competition, the Commission adopted in 2023 [Implementing Regulation](https://europa.eu/!PhF44v){:target="_blank"} (EU) 2023/138 laying down a list of concrete **high-value datasets (HVDs)** and the arrangements for their publication by the Member States. 

The main objective of establishing the HVDs list is to ensure that public data of highest socio-economic potential are made available for re-use with minimal legal and technical restriction and free of charge. 

HVDs are classified in six thematic **categories**: 1) geospatial; 2) earth observation and environment; 3) meteorological; 4) statistics; 5) companies and company ownership; and 6) mobility. 

As for their reuse conditions, HVDs should be made available for free, in a machine-readable format, via application programming interfaces (APIs) and bulk download.  

For further information, please check the Commission [Shaping Europe’s digital future fact pages](https://digital-strategy.ec.europa.eu/en/factpages){:target="_blank"}, and in particular this guide: [Open data and high-value datasets: step-by-step access guide](https://digital-strategy.ec.europa.eu/en/factpages/open-data-and-high-value-datasets-step-step-access-guide){:target="_blank"}.