##**Who ensures the reporting: Member State or the Commission?**

Under Article 5(3) of the [Implementing Regulation](http://data.europa.eu/eli/reg_impl/2023/138/oj) it is an obligation of each Member State to „provide the Commission with a report on the measures they have carried out to implement this Implementing Regulation“. The report has to be delivered by each Member State. That said, the tools made available by the Commission make the reporting simple and less onerous for Member States, e.g. thanks to the possibility to refer to concrete SPARQL queries and to their results. 

##**As a Member State: what do I need to do for a successful automated reporting?**

•	Ensure **coordination within your country**: identify the relevant catalogues (end points) that contain HVDs and notify them to the Commission (via an [EU Survey form](https://ec.europa.eu/eusurvey/runner/HVD-harvesting)), so that data.europa.eu can harvest their metadata.

•	Let the Commission know of your country’s contact person for HVDs: presumably the member of the PSI Group, unless you indicate otherwise.

•	If you want to use the recommended common reporting practice:

1.	Ensure availability of **metadata**, preferably in [DCAT-AP-HVD](https://interoperable-europe.ec.europa.eu/collection/semic-support-centre/solution/dcat-ap-hvd/release/300) format

2.	Ensure that metadata **harvesting** of all relevant **MS catalogues** by the DEU is done

3.	Use the [SPARQL queries](https://dataeuropa.gitlab.io/data-provider-manual/hvd/Reporting_guidelines_for_HVDs/), while minding the reporting **quality** (e.g. avoid duplication, persistency of identifiers, …) 

##**How should Member States transmit the results of SPARQL queries and any additional information to the European Commission?**

Member States can report their implementation status to the Commission by submitting/uploading the results of relevant queries and commenting on other points in an online EU Survey questionnaire, available to Member States in the course of January 2025. 

##**Is there an example of a report available?**

The core of the report will be the results of SPARQL queries. The SPARQL queries presented at the webinars in June and October 2024 (and available at <https://dataeuropa.gitlab.io/data-provider-manual/hvd/Reporting_guidelines_for_HVDs/>) are available to be tried and tested by anyone. As countries are still in the process of denoting high-value datasets on their national portals and making them accessible for harvesting, properly denoted high-value datasets are only gradually becoming available on data.europa.eu. 

##**Which codelist value shall MS use to tag the metadata of high-value datasets?**

Tagging HVDs is a precondition for HVD reporting using the tools made available by the Commission. Three elements have been standardised:
For denoting a dataset as HVD, you need to fill in the field ‘applicable legislation’ with the ELI of the [Implementing Regulation](http://data.europa.eu/eli/reg_impl/2023/138/oj) in <https://interoperable-europe.ec.europa.eu/collection/semic-support-centre/solution/dcat-ap-hvd/release/300>: dcatap:applicableLegislation <http://data.europa.eu/eli/reg_impl/2023/138/oj>. 

Secondly, there is a controlled vocabulary to be used when filling in the metadata to inform on HVD categories: <https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/high-value-dataset-category>. The Vocabulary includes also the ‘dataset’ level (i.e. titles of datasets as set out in the Annex to the Implementing Regulation).

Finally, for the purposes of the reporting it is also important to indicate in the metadata the information on APIs/bulk download and licences (ideally CC-BY).

##**The reporting queries are performed over catalogues using  DCAT-AP metadata model. What version?**

Until recently it was DCAT-AP version 2, after the update it is recommended to use [version 3](https://semiceu.github.io/DCAT-AP/releases/3.0.0/). The automated reporting will work with both DCAT-AP versions. For national catalogues that do not use DCAT-AP, mappings are needed between their metadata profiles and DCAT-AP to allow harvesting by data.europa.eu. The mapping is done automatically when datasets' metadata are harvested, and they are all in DCAT-AP format on data.europa.eu. Even though the mapping leads to a minor loss of information, the full information remains in the metadata on the national portal. Mapping to DCAT-AP is a precondition for HVD reporting using the tools made available by the Commission.
Besides the mapping that is done in the course of harvesting by data.europa.eu, there is a standalone tool for mapping INSPIRE metadata into DCAT-AP: Github XSLT for ISO INSPIRE to GeoDCAT-AP (<https://github.com/SEMICeu/iso-19139-to-dcat-ap>). 


##**When will national catalogues be harvested for the purpose of reporting?**

It depends on the arrangement between the Member State and data.europa.eu. The report is to be submitted by 9 February 2025. Many relevant catalogues have already been harvested. It is the responsibility of each Member State to inform data.europa.eu of the national catalogues that are relevant for reporting on HVDs and therefore need to be harvested. 
Data.europa.eu is technically prepared to harvest and host the High-Value Datasets (HVDs) metadata from national catalogues. These datasets can be displayed on data.europa.eu in a dedicated subcatalogue titled "High-Value Datasets," allowing users to filter by HVD categories and other metadata.
To ensure timely harvesting, please inform the Commission in due time which national catalogues from your country already contain annotated HVDs. This will allow checking the harvesting and see if the catalogues are aligned with rules applicable to HVDs.

##**How to report on the information required by article 5 (3) particularly d, e, f of the Implementing Regulation?**

This concerns information on possible guidance documents, data protection impact assessments and information on public sector bodies temporarily exempted from the non-charging principle. If Member States have anything to report on these points, they can mention it in an online EU Survey questionnaire, available in the course of January 2025, including the option to upload a document. 

 
