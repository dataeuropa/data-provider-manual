Under Article 5(3) of the [Implementing Regulation](http://data.europa.eu/eli/reg_impl/2023/138/oj){:target="_blank"} each Member State must „provide the Commission with a report on the measures they have carried out to implement this Implementing Regulation“.  It has to be submitted by 9 February 2025 and then **every 2 years**. 

The report has to contain the following information:

> (a) a list of published high-value datasets at Member State level (and, where relevant, subnational level) with online reference to metadata;

> (b) persistent link to the applicable licensing conditions;

> (c) persistent link to the APIs ensuring access to the high-value datasets; 

 If relevant, Member States should inform the Commission also about some other aspects, such as guidance documents or data protection impact assessments. 

The reporting information under letters a) to c) can be provided through references to relevant metadata. For that purpose, the Commission has made available tools to help make the reporting simple, thanks to concrete SPARQL queries that can be used on data.europa.eu. 