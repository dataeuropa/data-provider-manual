Organisation and publication of the EU Open Data Days

Reference: DPR-EC-00457.8

Entity of the Operational Controller: European Commission: Publications Office (OP.C.1)

Publication date: 10/07/2024





 **Table of Contents**

1. General information
2. Purpose and description of the processing
3. Data subjects and data categories
4. Retention period
5. Recipients
6. International data transfers
7. Information to data subjects on their rights
8. Security measures 


 <br>
 
## General information
     
**Data protection record**
**Record reference**
DPR-EC-00457.8

**Title of the processing operation**
Organisation and publication of the EU Open Data Days

**Language of the record**
English

**Corporate record**
No

**Data Protection Officer**
**Contact details**
[DATA-PROTECTION-OFFICER@ec.europa.eu](mailto:DATA-PROTECTION-OFFICER@ec.europa.eu)

**Entity of the Operational Controller**
**Responsible organisational entity**
Publications Office (OP)

**Directorate / Unit**
C.1

**Contact Details**
[OP-C4002@ec.europa.eu](mailto:OP-C4002@ec.europa.eu)

**Other Commission departments involved**
**Other Commission departments involved in the processing**
No

**Joint controllership**
**Joint controllership is involved**
N/A

**Processors**
**Processors are involved in the processing**
Yes

-  **Names and contact details of processors**
Event organiser: Pomilio Blumm Srl, Pescara, Lungomare Papa Giovanni XXIII n. 22, Italy

## PURPOSE AND DESCRIPTION OF THE PROCESSING

**Purpose**

**Description of the purpose of the processing**

The Publications Office organises the EU Open Data Days event in order to promote and foster the harnessing, reuse, and impact of open data and the related portal data.europa.eu.

The editions of the event are organised as conferences; either as physical live events or as online events on the networking platform EU Open Data Days or as a combination of both.

Personal data is collected to manage the registration of the participants and follow up with them, including invitations, feedback, regular newsletters and organisation of future events.

The personal data of participants in online versions of the event may also be processed by using a matchmaking tool using artificial intelligence (AI).

In addition, the Publications Office organises webinars with interested persons in preparation and follow up of an EU Open Data Days event.

For the transparency and the visibility of the event, participants' names, photos, videos, presentations and CVs are published ad hoc on the websites managed by the Publications Office (OP Portal and/or data.europa.eu) and the European Commission. They may also be published in relevant publications (event programme, event proceedings, articles about the event) and shared on social media (such as Facebook, LinkedIn, Twitter, Instagram and YouTube) and on communication channels of the European Commission and other EU institutions, agencies and bodies.

**Processing for further purposes**

**The purpose(s) for further processing**

- Archiving in the public interest
- Statistical purposes

**Safeguards in place to ensure data minimization**

- Any other:

Anonymisation

**Modes of processing**

**The mode of processing**

- Automatic processing
- Manual processing

**Description/additional information regarding the modes of processing**

The personal data of participants who agree to participate in AI-powered matchmaking/networking activities will be subject to automated processing by artificial intelligence (AI), using a contractor's tool.

Participants who do not wish to have their picture taken and/or do not wish be part of the web-streaming and recording/publishing activities carried out at the EU Open Data Days event have the possibility to opt out to this processing by informing the Publications Office. 

**Storage medium**

**The medium of storage (one or more)**

- Electronic

    - Digital (Office documents such as Word, Excel, PowerPoint, Adobe PDF, Audiovisual/multimedia assets, Image files such as .JPEG, PNG)

    - Databases

    - Servers

    - Cloud
    
- Others:

OP website, data.europa.eu, other European Commission web sites and social media networks

**Description/additional information regarding the storage medium**

 --

**Source of personal data**
**Personal data are obtained directly from the data subjects**
Yes

**Comments**
**Comments/additional information on the data processing**
Linked records: DPR-EC-02096 - Data.europa.eu DPR-EC-00449 - User registration on “Publications Office of the European Union” website (OP Portal)

## DATA SUBJECTS AND DATA CATEGORIES

**Data subjects’ categories**

**Data subject(s) are**

- Internal to the organisation

**A description of the data subjects (internal to the organisation)**

Contributors, where they are EU staff, natural persons who submit a project description to participate in the call for contributions as speakers.

Visitors, where they are EU staff, who have registered to be part of the event.

Appointed programme committee members, where they are EU staff.

- External to the organisation

**A description of the data subjects (external to the organisation)**

Contributors, where they are not EU staff: natural persons who submit a project description to participate in the call for contributions as speakers; also other speakers and professionals contributing to the event such as the moderator

Visitors and speakers, where they are not EU staff, who have registered to attend the event

Appointed programme committee members, where they are not EU staff

**Data categories/fields**

**Description of the categories of data that will be processed**

**EU Open Data Days online participants/viewers** 

- Mandatory: first name, last name, organisation, position, email address
- Non-Mandatory: IP address.

Also, all browser-generated information (website data, geolocation, cookies or other technologies used to analyse users’ activity) is subject to consent except for necessary cookies.

**EU Open Data Days physical participants**

Mandatory: first name, last name, organisation, position, email address, country of residence

**EU Open Data Days speakers / presenters and other contributors**

Mandatory: first name, last name, organisation, position, email address, country of residence, professional bio information including previous works, photos

Non-Mandatory: social media accounts

**EU Open Data Days networking platform participants**

- Mandatory: First name, last name, organisation name, organisation type (incl. description of the organisation and areas of activities and interests), (professional/personal) e-mail address, country, city, IP address.

- Non-mandatory via the networking platform: Short biography, social media accounts, selected labels (areas of activities and interests), comments you decide to share, feedback, questions, contributions left during the event.

Also, all browser-generated information (website data, geolocation, cookies or other technologies used to analyse users’ activity) is subject to consent except for necessary cookies.

**Webinars.**

- Mandatory: First name, e-mail address, field of work

- Non-mandatory: family name

**The processing operation concerns any 'special categories of data' which fall(s) under Article 10(1), which shall be prohibited unless any of the reasons under Article 10(2) applies**

N/A

**Description/additional information regarding special categories of personal data**

--

**Data related to ‘criminal convictions and offences’**

**The data being processed contains sensitive data which fall(s) under Article 11 'criminal convictions and offences'**

N/A

**Comments**

**Comments/additional information on data subjects and data categories**
Registration for participants and visitors: https://op.europa.eu/en/web/euopendatadays

##  RETENTION PERIOD

**Data categories and their individual retention periods**

**The administrative time limit(s) for keeping the personal data per data category**

1. **Data category**

    Data of participants

    **Retention period**

    Max. 5 years from registration

    **Start date description**

    --

    **End date description**

    --

2. **Data category**

    Data collected on the EU Open Data Days networking platform

    **Retention period**

    No later than 6 months after the event

    **Start date description**

    --

    **End date description**

    --

**Comments**

**Comments/additional information on the data retention periods**

Data subjects' requests are treated within a maximum of 30 working days.

 

## RECIPIENTS

**Origin of the recipients of the data**

**The origin of the data recipients**

-  Within the EU organisation

**A description of the indicated recipients of the data**

Staff of the European Union institutions


-  Outside the EU organisation

**A description of the indicated recipients of the data**

Programme committee, where they are not EU staff.

Contractor organising the event, and service providers carrying out specific processing linked to the event, see under V.2 "Who has access to which parts of the data" below.

**Categories of the data recipients**

**The categories (one or more) of the data recipients**

- A natural or legal person
- Public authority
- Any other third party:

The Publications Office may share names of speakers and other contributors, photos and videos of the event, the presentations, event outcomes and other related information on EU websites, via mailings lists, newsletters, brochures (and other printed material) and on social media (such as Facebook, LinkedIn, Twitter, Instagram and YouTube). It may also share this information on the internal communication channels of the European Commission and other EU institutions, agencies and bodies as well as with external press.

**Description of the indicated category(ies) of data recipients**

Staff of the European Union institutions; external experts as programme committee; contractors.

**Who has access to which parts of the data**

Staff of the EU institutions organising the events have access to all data. The European Commission, the European Anti-Fraud Office and the Court of Auditors may carry out checks and audits in relation to the event. Programme committee and experts who are not EU staff have access to all personal data except financial data. Contractor and service providers: For the EU Open Days 2025, the contractor Pomilio Blumm Srl is recipient of personal data of registered participants and/or those sending emails to the shared inbox. Where necessary, Pomilio Blumm Srl may also share the information with service providers for the purpose of organising the EU Open Days 2021 activities. Service providers may change through editions, in which case this list is updated. - EventWorks (registration platform – mandatory): First name, last name, email address, professional life data (organisation, role/job title, country). The privacy policy can be found here. - Swapcard (networking platform - optional): First name, last name, email address, professional life data (organisation, role/job title, country). Swapcard privacy policy can be found here. - Slido (Q&A - optional): first name, last name, organisation, IP address (remains possible to submit anonymous). Slido privacy policy can be found here. - Wonder (virtual space - optional): First Name, last name, email address. Wonder privacy policy can be found here. - EUSurvey (survey - optional): First Name, last name, email address. EUSurvey privacy policy can be found here. - Walls.io (social wall - optional): Those active on social media may see their social media posts featured on the event social media wall and within in the livestream, which will be activate during and up to three weeks after the event. (privacy policy can be found here.) - Cisco WebEx (web conference – mandatory for speakers/moderators): first/last or screen name, email address, IP address, browser-generated information browser-generated information (including device information, operating system, device type, cookies or other technologies used to analyse users’ activity). It hosts the collected personal data on servers in the EU and has BCRs in place. The privacy policy can be found here. - Docusign (signature consent form – mandatory for speakers/moderators only): first name, last name, email address, signature on the recording authorisation form. The privacy policy can be found here. - Clevercast (livestream/video plater – mandatory to view event). The privacy policy can be found here.

**Comments**

**Comments/additional information on data recipients**

--
 

## INTERNATIONAL DATA TRANSFERS

**Transfer outside of the EU or EEA**

**Data is transferred to countries outside the EU or EEA**

N/A

**Transfer to international organisation(s)**

**Data is transferred to international organisation(s)**

N/A

**Comments**

**Comments/additional information on international data transfers**

**Slido:**

Slido is hosted on AWS (Amazon Web Services) infrastructure, located in the EU (Ireland, Germany). Technical data of participants may be transferred to Service Providers in third countries under the legal basis Article 50(1)(a) of Regulation (EU) 2018/1725 (the data subject has explicitly consented to the proposed transfer, after having been informed of the possible risks of such transfers for the data subject due to the absence of an adequacy decision and appropriate). [An overview of the Service Providers and data sent be found here.](https://www.sli.do/terms#service-providers)

**DocuSign:**

DocuSign has received the approval of the applications for Binding Corporate Rules (BCRs) as both a data processor and data controller from the European Union Data Protection Authorities. [More information here.](https://www.docusign.com/trust/privacy/binding-corporate-rules)

Use of DocuSign under the legal basis Article 50(1)(a) of Regulation (EU) 2018/1725 (the data subject has explicitly consented to the proposed transfer, after having been informed of the possible risks of such transfers for the data subject due to the absence of an adequacy decision and appropriate).

**WebEx (Cisco):**

Cisco has received the approval of the applications for Binding Corporate Rules (BCRs) as a data controller from the European Union Data Protection Authorities. [More information here.](https://www.cisco.com/c/en/us/about/trust-center/customer-data-privacy-policy.html) The account used by MCI Benelux SA to host webinars is hosted in Frankfurt, DE, EU. Use of WebEx under the legal basis Article 50(1)(a) of Regulation (EU) 2018/1725 (the data subject has explicitly consented to the proposed transfer, after having been informed of the possible risks of such transfers for the data subject due to the absence of an adequacy decision and appropriate).
 

## INFORMATION TO DATA SUBJECTS ON THEIR RIGHTS

**Privacy statement**

**Rights of the data subjects**

**The processing should respect the following rights of data subjects**

Article 17 - Right of access by the data subject

Article 18 - Right to rectification

Article 19 - Right to erasure (right to be forgotten)

Article 20 - Right to restriction of processing

Article 21 - Notification obligation regarding rectification or erasure of personal data or restriction of processing

Article 22 - Right to data portability

Article 23 - Right to object

Article 24 - Rights related to Automated individual decision making, including profiling


**The data subjects are informed about their rights and how to exercise them in the form of a privacy statement attached to this record**

Yes

**Publication of the privacy statement**

-  Published on website


**Guidance for Data subjects which explains how and where to consult the privacy statement is available and will be provided at the beginning of the processing operation**

Yes

**An explanation of the guidance on how and where to consult the privacy statement**

The privacy statement for the EU Open Data Days will be available online. When data subjects are contacted by email, a link to the privacy statement will be provided at the bottom of the message.

Participants/contributors/visitors who do not want their images/video recordings to be made available online can opt out by sending an email to the indicated generic mailbox. 

**The privacy statement(s)**

- [DPR-EC-00457_privacy_statement_20240709.pdf](../assets/dpr-ec-00457_privacy_statment_20240709.pdf)

**Comments**

**Comments/additional information on information to data subjects on their rights**

--

 

## SECURITY MEASURES

**Short summary of overall Technical and Organisational measures implemented to ensure Information Security:**

The internal processing is only carried out on servers of the Publications Office. The IT systems of the Publications Office are managed by DIGIT.

The external contractor is bound by strict IT security rules fixed in the contract.

The Commission’s contractors are bound by a specific contractual clause for any processing operations of your data on behalf of the Commission, and by the confidentiality obligations deriving from the General Data Protection Regulation (‘GDPR’ Regulation (EU) 2016/679).