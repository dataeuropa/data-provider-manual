A cookie is a small text file that a website saves on your computer or mobile device when you visit a site. It enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period, so you do not have to keep re-entering them whenever you come back to the site or browse from one page to another.

To make this site work properly, the service sometimes places cookies on your device.


## How do we use cookies?
Several of our pages use cookies to remember:

your display preferences, such as contrast colour settings or font size;
- if you have already indicated in a popup your preference to not see it again ;
- if you have agreed (or not) to our use of cookies on this site.
Also, some videos embedded in our pages use a cookie to anonymously gather statistics on how you got there and what videos you visited.

Enabling these cookies is not necessary for the website to work but it will provide you with a better browsing experience. You can delete or block these cookies, but if you do that some features of this site may not work as intended.

The cookie-related information **is not used to identify you personally** and the pattern data is fully under our control. These cookies are not used for any purpose other than those described here. 


## Do we use other cookies?

Some of our pages or subsites may use additional or different cookies to the ones described above. If so, the details of these will be provided in their specific cookies notice page. You may be asked for your agreement to store these cookies.


## How to control cookies

You can **control and/or delete** cookies as you wish. You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site, and some services and functionalities may not work.
