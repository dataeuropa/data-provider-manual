European Union, 1995–2024 


The Commission's reuse policy is implemented by [Commission Decision 2011/833/EU of 12 December 2011 on the reuse of Commission documents](https://eur-lex.europa.eu/eli/dec/2011/833/oj). 

Unless otherwise noted (e.g. in individual copyright notices), the reuse of the editorial content on this website owned by the EU is authorized under the [Creative Commons Attribution 4.0 International (CC BY 4.0) licence](https://creativecommons.org/licenses/by/4.0/). This means that reuse is allowed, provided appropriate credit is given and any changes are indicated. 

You may be required to clear additional rights if a specific content depicts identifiable private individuals or includes third-party works. To use or reproduce content that is not owned by the EU, you may need to seek permission directly from the respective rightholders. Software or documents covered by industrial property rights, such as patents, trademarks, registered designs, logos and names, are excluded from the Commission's reuse policy and are not licensed to you. 

To the extent possible under law, the European Union has waived all copyright and related or neighbouring rights to metadata of the open data portal via [Creative Commons CC0 1.0 Universal  Public Domain Dedication deed](https://creativecommons.org/publicdomain/zero/1.0/).

 

Most of the resources published display a specific reference to the licence under which the owner has chosen to release them. 

For the resources without licence information, users must consult the licence conditions in the original portal where the resources were initially published.

For all copyright issues, please contact: [op-copyright@publications.europa.eu](mailto:op-copyright@publications.europa.eu).
