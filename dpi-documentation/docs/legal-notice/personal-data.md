PROTECTION OF YOUR PERSONAL DATA

This privacy statement provides information about
the processing and the protection of your personal data.

 

Processing operation: Data.europa.eu 

Data Controller: Publications Office of the European Union, unit C.1, 'European Data and Related Services'

Record reference: DPR-EC-02096





 **Table of Contents**

1. Introduction
1. Why and how do we process your personal data?
1. On what legal ground(s) do we process your personal data?
1. Which personal data do we collect and further process?
1. How long do we keep your personal data?
1. How do we protect and safeguard your personal data?
1. Who has access to your personal data and to whom is it disclosed?
1. What are your rights and how can you exercise them? 
1. Contact information
1. Where to find more detailed information?

 <br>
 
## Introduction
     
The European Commission (hereafter ‘the Commission’) is committed to protect your personal data and to respect your privacy. The Commission collects and further processes personal data pursuant to [Regulation (EU) 2018/1725](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2018.295.01.0039.01.ENG&toc=OJ:L:2018:295:TOC) of the European Parliament and of the Council of 23 October 2018 on the protection of natural persons with regard to the processing of personal data by the Union institutions, bodies, offices and agencies and on the free movement of such data (repealing Regulation (EC) No 45/2001).

This privacy statement explains the reason for the processing of your personal data, the way we collect, handle and ensure protection of all personal data provided, how that information is used and what rights you have in relation to your personal data. It also specifies the contact details of the responsible Data Controller with whom you may exercise your rights, the Data Protection Officer and the European Data Protection Supervisor.

The information in relation to processing operation 'data.europa.eu' undertaken by unit C.1, 'European Data and Related Services' of the Publications Office of the European Union, is presented below. 


## Why and how do we process your personal data?

Purpose of the processing operation: Unit C.1, 'European Data and Related Services' of the Publications Office collects and uses your personal information to operate ata.europa.eu.

The following specific processing operations are carried out:


- identify authorised data providers and their operations and guarantee that only they can insert and modify their data;

- send information to data providers about the development and use of the website;

- send information and new datasets suggested by users;

- allow users to submit open data use cases via Use case forms

- send a newsletter to users who have expressed their explicit interest in receiving it (newsletter distribution list);

- carry out surveys concerning the use of the portal;

- organise and conduct Webinars (via Webex) and other training sessions on using the portal, incl. follow-up. 

- In addition, user requests received via the Helpdesk may be transferred to other Commission DGs or EU institutions (as a rule, the data provider).

Your personal data will not be used for an automated decision-making including profiling.

## On what legal ground(s) do we process your personal data

We process your personal data, because:

- processing is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the Union institution or body;

and

- you as data subject have given your consent to the processing of your personal data for one or more specific purposes.

Further legal bases for the processing: 


- Directive 2003/98/EC of the European Parliament and of the Council of 17 November 2003 on the re-use of public sector information (as amended by Directive 2013/37/EU), esp. Article 9
- Commission Decision 2011/833/EU on the reuse of Commission documents
- Decision 2009/496/EC, Euratom of the European Parliament, the Council, the Commission, the Court of Justice, the Court of Auditors, the European Economic and Social Committee and the Committee of the Regions of 26 June 2009 on the organisation and operation of the Publications Office of the European Union.
 

## Which personal data do we collect and further process?

In order to carry out this processing operation unit C.1, 'European Data and Related Services' of the Publications Office collects the following categories of personal data:

Users who are authorised data providers:


- Full name, user ID, email address, password, organisation (which provides the data); this information is needed to access personalised features like the storing of favourite datasets and favourite SPARQL queries;

If you are a participant in an online training courses and/or webinar:

- Name, email address, and date, time and duration of your participation

When you contact the Helpdesk:

- Emails may contain (apart from the email address) first name and surname, administrative address, telephone, fax and organisation. 


If you suggest a new dataset:

- Mandatory: name and email address, object and description of proposed dataset.
- Optional: country, language and type of organisation.

The provision of personal data is mandatory for the respective functionalities of Data.europa.eu.

 

## How long do we keep your personal data?

Unit C.1, 'European Data and Related Services' of the Publications Office only keeps your personal data for the time necessary to fulfil the purpose of collection or further processing, namely for 2 years after the last active use. 

 

## How do we protect and safeguard your personal data?

All personal data in electronic format (e-mails, documents, databases, uploaded batches of data, etc.) are stored either on the servers of the European Commission or of its contractors. All processing operations are carried out pursuant to the [Commission Decision (EU, Euratom) 2017/46](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1548093747090&uri=CELEX:32017D0046) of 10 January 2017 on the security of communication and information systems in the European Commission.

The Commission’s contractors are bound by a specific contractual clause for any processing operations of your data on behalf of the Commission, and by the confidentiality obligations deriving from the General Data Protection Regulation (‘GDPR’ [Regulation (EU) 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32016R0679)).

In order to protect your personal data, the Commission has put in place a number of technical and organisational measures in place. Technical measures include appropriate actions to address online security, risk of data loss, alteration of data or unauthorised access, taking into consideration the risk presented by the processing and the nature of the personal data being processed. Organisational measures include restricting access to the personal data solely to authorised persons with a legitimate need to know for the purposes of this processing operation.

 

## Who has access to your personal data and to whom is it disclosed?

Access to your personal data is provided to the Commission staff responsible for carrying out this processing operation and to authorised staff according to the “need to know” principle. Such staff abide by statutory, and when required, additional confidentiality agreements.
Additionally, Helpdesk requests, including the personal data contained in them, may be forwarded to staff of another Commission DG or EU institution or body.
Specific tasks are carried out by the contractor.
Consortium leader: Capgemini Invent NL, consortium partner: INTRASOFT International.

Subcontractors:

- 52 North (geospatial data technology)
- con terra (geospatial data technology)
- Agiledrop (content management systems)
- Prof. Elena Simperl (independent, King's College London) and University Politecnica de Madrid (data research and studies)
- Fraunhofer FOKUS (data technology)
- The Lisbon Council (socio-economic research and studies)
- OMMAX (SEO)
- Timelex (legal research and studies)

The portal is hosted on Amazon Cloud (Amazon Web Services, contract administered by DIGIT).

 

## What are your rights and how can you exercise them?

You have specific rights as a ‘data subject’ under Chapter III (Articles 14-25) of Regulation (EU) 2018/1725, in particular the right to access, rectify or erase your personal data and the right to restrict the processing of your personal data. Where applicable, you also have the right to object to the processing or the right to data portability.

You have the right to object to the processing of your personal data, which is lawfully carried out pursuant to Article 5(1)(a). 

You have consented to provide your personal data to Unit C.1, 'European Data and Related Services' of the Publications Office for the present processing operation. You can withdraw your consent at any time by notifying the Data Controller. The withdrawal will not affect the lawfulness of the processing carried out before you have withdrawn the consent.
You can exercise your rights by contacting the Data Controller, or in case of conflict the Data Protection Officer. If necessary, you can also address the European Data Protection Supervisor. Their contact information is given under Heading 9 below. 


Where you wish to exercise your rights in the context of one or several specific processing operations, please provide their description (i.e. their Record reference(s) as specified under Heading 10 below) in your request.

 

## Contact information

-  The Data Controller   
If you would like to exercise your rights under Regulation (EU) 2018/1725, or if you have comments, questions or concerns, or if you would like to submit a complaint regarding the collection and use of your personal data, please feel free to contact the Data Controller, Unit C.1, 'European Data and Related Services' of the Publications Office, [info@publications.europa.eu](mailto:info@publications.europa.eu).

- The Data Protection Officer (DPO) of the Commission

You may contact the [Data Protection Officer](mailto:DATA-PROTECTION-OFFICER@ec.europa.eu) with regard to issues related to the processing of your personal data under Regulation (EU) 2018/1725.

- The European Data Protection Supervisor (EDPS)

You have the right to have recourse (i.e. you can lodge a complaint) to the European Data Protection Supervisor [edps@edps.europa.eu](mailto:edps@edps.europa.eu) if you consider that your rights under Regulation (EU) 2018/1725 have been infringed as a result of the processing of your personal data by the Data Controller.

 

## Where to find more detailed information?

The Commission Data Protection Officer (DPO) publishes the register of all processing operations on personal data by the Commission, which have been documented and notified to him. You may access the register via the following link: [http://ec.europa.eu/dpo-register](http://ec.europa.eu/dpo-register).

This specific processing operation has been included in the DPO’s public register with the following Record reference: DPR-EC-02096.
