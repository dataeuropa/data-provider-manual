The metadata catalogue of datasets can be explored through a search engine ([data tab](https://data.europa.eu/data/datasets)), through a [SPARQL endpoint](https://data.europa.eu/data/sparql?locale=en) and [API endpoint](https://data.europa.eu/api/hub/search/).

An up-to-date list of all catalogues can always be retrieved by this SPARQL [query](https://data.europa.eu/sparql?default-graph-uri=&query=prefix+dct%3A+++%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E+%0D%0Aprefix+dcat%3A++%3Chttp%3A%2F%2Fwww.w3.org%2Fns%2Fdcat%23%3E+%0D%0A%0D%0Aselect+%3FcatalogURI++%3FcatalogName++%3FcatalogDescription%0D%0Awhere+%7B%0D%0A++%7B%0D%0A++++%3FcatalogURI+a+dcat%3ACatalog+.%0D%0A++++%3FcatalogURI+dct%3Atitle+%3FcatalogName+.%0D%0A++++%3FcatalogURI++dct%3Adescription+%3FcatalogDescription%0D%0A++%7D%0D%0A%7D&format=text%2Fhtml&timeout=0&signal_void=on).

## Search manually

### Basic Search

You can search for datasets containing one or more keywords by typing the keywords into the search field and then clicking on the search button. The search process will reduce your keywords to their root form, to ensure variants of your keyword also match. For example, "*Europa*" can also match "*Europe*", or "*walking*" can also match "*walk*" and "*walked*".

#### Responsive filtering

The search interface is extended with an overall section filter for datasets, catalogues, and editorial content above the search field on the single search page. Depending on the selected section filter, additional filters become available based on the filtered section.

![Responsive filtering](../images/search-for-datasets/responsive-filtering.png)

### Advanced Search

When entering more than one search term it is also possible to use the logical operators (connectors) **AND**, **OR** as well as **parentheses ()**. Furthermore, it is also possible to do a **wildcard** search and search for an **exact phrase**.

**Logical Operator OR**

The **OR** operator can be used in order to

- connect two or more similar concepts (synonyms)
- broaden your results, telling the database that ANY of your search terms can be present in the resulting dataset

Example: *population OR education OR science*

All three circles represent the result set for this search. It is a big set because any of those words are valid using the OR operator.

![Logical Operator OR](../images/search-for-datasets/logical-or.png)

**Logical Operator AND**

The **AND** operator can be used to:

- find sources containing two or more ideas
- narrow the search

The database will only retrieve items containing both keywords. The **AND** operator can be used multiple times in one query.

Example: *population AND education AND science*

![Logical Operator AND](../images/search-for-datasets/logical-and.png)

**Parantheses ()**

Parentheses are used to:

- perform a more complex search using both AND and OR by placing parentheses around synonyms
- save time by searching multiple synonyms at once

Example: *(environment OR nature) AND (refurbish OR reuse)*

This avoids the need to perform multiple searches for combinations of keywords.

![Parentheses](../images/search-for-datasets/parentheses.png)

**Basic rules for using AND, OR operators and parentheses ()**

- The OR is implicit: search function automatically puts an OR in between your search terms. It means that the search for population OR education OR science gets the same matches as the search for population education science.

![cat OR dog](../images/search-for-datasets/cat-or-dog.png)

![cat dog](../images/search-for-datasets/cat-dog.png)

- Always enter **AND** and **OR** operators in uppercase letters.
- Never translate **AND** and **OR** operators into any other language. It does not matter in what language your keywords are the operators always must be **AND** and **OR**.

![katze OR hund](../images/search-for-datasets/katze-or-hund.png)

![katze ODER hund](../images/search-for-datasets/katze-oder-hund.png)

- Please keep in mind to always close **parentheses ()**. The combination of logical operators with an odd number of **parentheses** in the query leads to incorrect results in the search.

![(cat AND dog) OR bird](../images/search-for-datasets/cat-and-dog-or-bird.png)

![(cat AND dog) OR bird)](../images/search-for-datasets/cat-and-dog-or-bird_wrong.png)

**Wildcard**

Wildcard search can be used to optimize the search result when you do not know the entire keyword by using ? (question mark) to replace a single character and * (asterisk) to replace zero or more characters.

Example: *ois?au dat\**

The example above will return datasets that can contain *oisiau data*, *oisoau dataset*, or *oiseau datasets*. Wildcard search does not reduce a keyword to its root form, like in basic search, since it would be wrong to do so on a term that some of its letters are unknown.

Please note that the more keywords need to be checked, just in case they match (e.g. *a\**, or *b\**, or *c\**), the heavier and the poorer the wildcard search performance can be. Having a wildcard at the beginning of a keyword (e.g. *\*ing*, or *?iseau*) is ignored and the search term will be treated as it is to avoid such a heavy and expensive process. Invalid wildcard search terms (e.g. *ois?au ****OR**** (data\* ****AND**** organization* (unbalanced parentheses)) will be also treated as it is.

**Exact Phrase**

When you place your keywords in double quotes, they will be considered as a phrase, not case sensitive and characters are taken literally as it is. The search will return datasets containing the keywords in exactly the same order.

Example: *"Manual public space"*

The example above returns datasets containing exactly “*manual public space*”. It will not return datasets containing “*manual space public*”, or *“space public manual*”, or any other results with the search terms appearing in different sequences than “manual-public-space”. If you search for "*public spa\**", the results will only list datasets containing “*public spa\**”.


## Terms of reuse

Most of the data accessible via data.europa.eu is released by the respective data providers using an open licence. Data can be used for free for commercial and non-commercial purposes, provided the source is acknowledged. Specific conditions for reuse, relating mostly to the protection of data privacy and intellectual property, apply to a small amount of data. A link to these conditions can be found for each dataset.

The terms of use can be found in the data.europa.eu [copyright notice](https://dataeuropa.gitlab.io/data-provider-manual/legal-notice/copyright/). Most data is covered by open licences. As of September 2021, the most common open licences were the [Creative Commons](https://en.wikipedia.org/wiki/Creative_Commons_license) ‘CC‑BY‑4.0’ licence, the ‘Data licence Germany – attribution’ licence or Etalab’s Open Licence (used by the French government).

![Terms of reuse](../images/search-for-datasets/reuse.jpg)
