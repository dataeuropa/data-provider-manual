# SPARQL search (graph search)

SPARQL search enables even more advanced users to find datasets using [Resource Description Framework (RDF)](https://www.w3.org/RDF/) query language. SPARQL can help to find specific information from a large amount of RDF dataset, even if it organized in a complex way. For more information see the [data.europa.eu SPARQL](https://data.europa.eu/en/about/sparql), and the [SPARQL search](https://data.europa.eu/data/sparql?locale=en).
