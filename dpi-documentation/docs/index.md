!!! information
    You can download and share the latest version of this documentation in the PDF format: [Download](pdf/documentation_data-europa-eu_V1.2.pdf)

[The portal](http://data.europa.eu/) is a central point of access to European open data from international, European Union, national, regional, local and geodata portals. It consolidates the former EU Open Data Portal and the European Data Portal.

The portal is intended to:

1. give access and foster the reuse of European open data among citizens, business and organisations.
1. promote and support the release of more and better-quality metadata and data by the EU’s institutions, agencies and other bodies, and European countries, enhancing the transparency of European administrations.
1. educate citizens and organisations about the opportunities that arise from the availability of open data.

![Unique features of data.europa.eu](./images/who-we-are/unique-features-of-deu.jpg)

The two former portals EU Open Data Portal and European Data Portal, launched respectively in 2012 and 2015, were originally established on the basis of [Directive 2003/98/EC](https://eur-lex.europa.eu/legal-content/en/ALL/?uri=CELEX%3A32003L0098) to promote accessibility to and the reuse of public sector information. The successor directives [Directive 2013/37/EU](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32013L0037) and [Directive (EU) 2019/1024](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32019L1024) confirmed and extended its action. The latter invites all the EU Member States to make available their public data resources, contributing to the smooth functioning of the internal market and the proper development of the information society in the EU.

Currently in its third iteration, the portal merges the activities of the European Data Portal (which focused exclusively on EU Member States and other European countries) and of the EU Open Data Portal (which served data from the EU institutions, agencies, and bodies) into one.

It is funded by the EU and managed operationally by the [Publications Office of the European Union](https://op.europa.eu/en/home) in cooperation with the [Directorate-General for Communications Networks, Content and Technology](https://commission.europa.eu/about-european-commission/departments-and-executive-agencies/communications-networks-content-and-technology_en) of the [European Commission](https://commission.europa.eu/index_en), responsible for EU open data policy.

![Data users](./images/who-we-are/data-users.jpg)

## Our mission, vision and values

The mission, vision and values of data.europa.eu are part of those from the Publications Office of the European Union (OP) with respect to open data.

OP mission. The Publications Office of the European Union is the official provider of publishing services to all EU institutions, bodies, and agencies. As such, it is a central point of access to EU law, publications, open data, research results, procurement notices and other official information. Its mission is to support EU policies and ensure that this broad range of information is available to the public as accessible and reusable data to facilitate transparency, economic activity, and the diffusion of knowledge.

OP vision. A well-informed EU, empowered by timely and effective access to trustworthy information and knowledge and benefiting from all the opportunities this brings to society and the economy.

OP values.

1. Transparency -- we facilitate transparency throughout the policy cycle of the EU institutions to enhance evidence-based decision-making, accountability, civic participation, and democracy.
1. Trustworthiness -- we strive to ensure that the content we provide is accurate and reliable so that citizens trust the EU as a provider of information.
1. Accessibility -- we believe access to information is a human right that all citizens should enjoy regardless of language, culture, disability, social status, location, technology, or the way they understand information.
1. Service orientation -- we are committed to continuously improving our services to both our institutional stakeholders and EU citizens because we want to contribute to the European project in the best possible way.

![OP values](./images/who-we-are/op-values.jpg)

## Translated metadata catalogue

The portal is a metadata catalogue. To foster the comparability of data published across borders, it presents metadata references using the application profile for data portals in Europe ([Data Catalogue Vocabulary (DCAT-AP)](https://joinup.ec.europa.eu/asset/dcat_application_profile/description)), using Resource Description Framework (RDF) technology. It provides translations of metadata descriptions in all 24 official EU [languages](https://europa.eu/european-union/about-eu/eu-languages_en) using machine-translation technologies ([eTranslation](https://webgate.ec.europa.eu/etranslation/public/welcome.html)).

In some situations, metadata machine-based translation might not be as efficient as human translation.

![DCAT-AP](./images/who-we-are/dcat-ap.jpg)

## From raw data to services

Our services are focused around three main spheres:

- *Data providers* -- any entity that gives access and distributes data to the public. To ensure the reliability of the published resources in the portal, the data providers of the portal are official representatives from supranational, national and local public administration. 
- *Data users* -- any person or entity who accesses and consumes data for any purpose. These include a wide range of data users who visit the portal for different purposes: non-governmental organisations, international organisations, private sector, academia, students, etc.
- *Data literacy* -- the ability to read, understand, create, and communicate data as information. Much like literacy as a general concept, data literacy focuses on the competencies involved in working with data.

The ‘data providers’ (EU institutions, agencies and EU bodies, Member States, and other European countries) are autonomous in publishing their metadata (which gives you access to their data) in data.europa.eu. The portal also publishes datasets of organisations beyond the EU. The portal is being updated whenever new datasets and content are available.

In addition to the vast collection of public datasets, most of them published in open formats, we offer additional contents and related services:

- training materials on open data in the [data.europa academy](https://data.europa.eu/en/academy)
- studies such as *[Open Data Maturity](https://data.europa.eu/en/impact-studies/open-data-maturity)* or *[The Economic Impact of Open Data](https://data.europa.eu/en/impact-studies/open-data-impact)*;
- [news](https://data.europa.eu/en/news-events/news) and [data stories](https://data.europa.eu/en/news-events/datastories)
- a [metadata quality dashboard](https://data.europa.eu/mqa/?locale=en) assessing metadata quality against various findable, accessible, interoperable, and reusable (FAIR) indicators
- a [licencing assistant](https://data.europa.eu/en/training/licensing-assistant)
- the possibility of storing your data under certain conditions (please [contact us](https://data.europa.eu/en/feedback/form))
- the social media channels ([X](https://twitter.com/EU_opendata), [LinkedIn](https://www.linkedin.com/company/publications-office-of-the-european-union), [YouTube](https://www.youtube.com/c/PublicationsOffice), [Facebook](https://www.facebook.com/data.europa.eu))
- a [list of events](https://data.europa.eu/en/news-events/events) with the most relevant events taking place
- a monthly [Newsletter](https://data.europa.eu/en/news-events/newsletter)

New features for registered users will be added soon to data.europa.eu. 

The whole source code of the portal is available at [GitLab](https://gitlab.com/dataeuropa) for free reuse.

How to contribute to the portal: 
- Give us [feedback](https://data.europa.eu/en/feedback/form?type=feedback-suggestions), for example, suggest new functionalities you would like to see on the portal
- [Suggest a portal to be harvested by us](https://data.europa.eu/en/about/add-your-open-data-catalogue)
- [Share your data use story](https://data.europa.eu/impact-studies/tell-us-your-story) in which we can promote the use of data

## Open data in the European Union

Several policy and legal developments on EU open data have taken place since 2003.

- **2003** -- [Directive 2003/98/EC](https://eur-lex.europa.eu/legal-content/en/ALL/?uri=CELEX%3A32003L0098) on the re-use of the public sector information (PSI) directive.
- **2011** -- [Commission Decision (2011/833/EU)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32011D0833) on the reuse of Commission documents, established the basis for the former EU Open Data Portal, operated by the [Publications Office of the EU](https://op.europa.eu/en/home) (2012--2021).
- **2012** -- launch of the EU Open Data Portal, central point of access to open data from EU institutions, agencies, and bodies.
- **2013** -- [Directive 2013/37/EU](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32013L0037) amending Directive 2003/98/EC on the re-use of public sector information.
- **2015** -- launch of the European Data Portal publishing data from national, regional, and local open data portals.
- **2019** -- [Directive 2019/1024](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32019L1024) on open data and the re-use of public sector information recasts Directives [2003/98/EC](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32003L0098&from=en) (PSI directive) and [2013/37/EU](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0037&from=EN) setting up a legal framework of minimum requirements for Member States regarding the accessibility of public data resources. 
- **2020** -- [Communication on a European strategy for data](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A52020DC0066) (COM/2020/66) aiming at creating a single market for data for the benefit of businesses, researchers, and public administrations to ensure Europe’s global competitiveness.
- **2021** -- [data.europa.eu](https://data.europa.eu) consolidates the former European Data Portal (data from the EU Member States and other European countries) and the former EU Open Data Portal (data from the EU institutions, agencies, and bodies) into the official portal for European data.
- **2022** -- the [Data Governance Act](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32022R0868&from=EN) sets a framework for fostering voluntary data sharing beyond open data 
- **2023** -- Entry into force of a [Commission implementing regulation on high-value datasets](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2023.019.01.0043.01.ENG), those that have a high potential for economic and societal impact

![Brief perspective](./images/who-we-are/brief-perspective.jpg)

## European open data space

The European open data space is an essential element of the single market for data -- an EU-wide interoperable data space that will enable the development of new products and services based on public data and industrial and scientific applications. It focuses on the implementation of EU open data and reuse policies under the legal acts adopted by the EU institutions. The Publications Office of the EU works on all four European open data space building blocks and their objectives:
- providing a comprehensive catalogue of open data and citizen-centric reuse services;
- improving the interlinking and interoperability of open data with other sources of public-sector information, such as legislation, publications and digital content;
- fostering the use of data from EU content through the organisation of [EU Datathon](https://op.europa.eu/en/web/eudatathon) competitions and data visualisation events;
- contributing to the implementation of data governance and policies across the EU institutions.

![Federated European Open Data](./images/who-we-are/federated-european-open-data.jpg)
