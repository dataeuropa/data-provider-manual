The Publications Office of the European Union maintains this website to enhance public access to information about its initiatives and European Union policies in general. Our goal is to keep this information timely and accurate. If errors are brought to our attention, we will try to correct them. However, the Publications Office accepts no responsibility or liability whatsoever regarding the information on this site.

This information is:

- of a general nature only and is not intended to address the specific circumstances of any individual or entity;
- not necessarily comprehensive, complete, accurate or up to date;
- sometimes linked to external sites over which the European Commission services have no control and for which the European Commission assumes no responsibility;
- not professional or legal advice (if you need specific advice, you should always consult a suitably qualified professional).

Please note that it cannot be guaranteed that a document available online exactly reproduces an officially adopted text. Only the Official Journal of the European Union (its printed edition, or since 1 July 2013 its electronic edition made available on the EUR-Lex website), is authentic and produces legal effects.
