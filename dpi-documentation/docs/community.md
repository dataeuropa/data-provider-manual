# The data.europa.eu collaboration channel

The [collaboration channel](https://data.europa.eu/dashboard/en/community/collaboration-channel){:target="_blank"} is a dedicated space for connecting and engaging in discussion with data providers and data reusers. You can access the [community guidelines](https://data.europa.eu/dashboard/sites/dashboard/files/2025-03/Community%20guidelines-%20%20collaboration%20channel.pdf){:target="_blank"} of the collaboration channel for more information.

By joining the collaboration channel, users will have the chance to:

- Join ongoing discussions to exchange ideas and experiences with peers, private sector representatives, and data enthusiasts. 
- Propose their own topics and conversations on subjects related to open data.
- Connect, collaborate and grow within the open data community network.

<br>

**How to join the collaboration channel**

To join this community, log in to your personal space on data.europa.eu. If you do not have an account, you will have to create one using EU Login.

 ![how to join](../images/community/how-to-join.png)

After you are logged in, you can request to join:

 ![request to join](../images/community/request-to-join.png)


1. **Data providers.** This is a **restricted** group designated to data providers who make their data available on data.europa.eu. If you are an official representative from supranational, national, regional and local public administrations, you can request access via the ‘Join’ button. The community managers will analyse your request, and if appropriate, you will be admitted in due course.

2. **Data reusers.** This is an **open** group for all users, and we also invite data providers to join and connect with data reusers. Data reusers are individuals or entities who access and use data for various purposes. This group encompasses a diverse range of users, including non-governmental organisations, international organisations, private sector companies, academic institutions, students and more. You can freely join the ‘Data reusers’ group by simply clicking the blue ‘Join’ button located in the discussions tab.

<br>

**How to contribute**

After joining the chosen group, users can contribute to the discussion in multiple ways.


1. Joining and following topics

    **Topics** are specific areas of interest for a given group (data providers or data reusers) and encompass multiple conversations on the given topic.
    > - You can join and follow topics with notifications via email or by receiving notifications via the personal space of data.europa.eu.
    > - If needed, you can also propose **new topics** for the group. Your proposal is sent to the community managers for consideration.

2. Contributing to or proposing conversations

    **Conversations** are threads within a given topic where you can find interactions or dialogues between users of a given group (data providers or data reusers).
    > - You can contribute to already-created conversations on a given topic.
    > - To suggest a new conversation users can click on the ‘Propose a conversation’ button located at the top of the respective group’s conversation page.
    > - This opens a contact form which allows the user to send a request for a new conversation.
    > - The proposal is then sent to the community managers for consideration (this does not apply to the members of the data providers group – they are allowed to add a new conversation in their community without any validation from the community managers).

3. Adding comments

    You can comment on each conversation and reply to your peers.
    > - Users can reply to comments by clicking the ‘Reply’ button located under the chosen comment.
    > - You can reply to, like or report a comment.
    > - Users can share a selected comment by clicking the ‘Share a comment’ button located under the author’s username.

**How to be updated**

To stay updated on the latest news on topics that interest you, you can follow those topics by clicking on the ‘follow’ button. 

![how to be updated](../images/community/how-to-be-updated.png)

You will be able to follow all activities of the **topics** (e.g. data reuse) or specific **conversations** created in each topic. Furthermore, you will be able to decide how to receive your customised notifications:
- You can receive them via e-mail on the latest content.
- You can receive them via the personal space on data.europa.eu, that you will only see when you’re logged in to the portal.

 ![follow](../images/community/follow.png)

To change these settings, visit the notifications management center by accessing your account in the top right corner of the website. In “My notifications” you can activate or change your notification preferences.

 ![my notifications](../images/community/my-notifications.png)

 ![notification management](../images/community/notification-management.png)

<br>

**How to change your preferences**

The personal dashboard of data.europa.eu is a space where you can manage your data.europa.eu’s account. To access it, please click on your username located in the top right corner of the website. 

 ![personal dashboard](../images/community/personal-dashboard.png)

In the personal dashboard users can:

- Edit their profile information, including their profile picture, personal social media account links, profile description;
- See conversations and topics they have proposed and manage their subscriptions;
- Set configurations and alerts, including configuring notifications (on-page or by email) for topics and conversations they follow, or marking notifications as read or deleted.
