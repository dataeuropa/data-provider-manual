There is no need to include a level one heading with one hashtag - that is done automatically by the MKDocs Framework based on the definition in the mkdocs.yml file.

## Second level headings start with two hashtags

I am a paragraph, there is nothing special about me, just a of lot Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sagittis dapibus lectus, sed venenatis velit fringilla volutpat. Integer egestas commodo lacus sit amet vestibulum. Sed tristique orci ut turpis condimentum, quis aliquam sapien vestibulum.

## What else can I do?

There are lots of things you can do.

### Third level headers, with three hashtags

You can go six levels deep - each level has an additional hashtag.

### Text formatting

You can make things **bold** with two asteriks either side. Or _italic_ by using an underscore or *just one asteriks*.

### What if I want underscores, hashtags, or asteriks?

Everything is treated as markdown, unless you escape it with a backslash.

This is *italic*, but this \*isn't\*.

## Links

Links need two things - some text that will act as a link, and a URL. For example, you can go to [Google](https://www.google.com) with "Google" inside square brackets immediately followed by the URL in regular brackets.

Alternatively, you can link to a page within the Data Providers Manual by using the "relative" path, such as /our-metadata-model to make [Our Metadata model](./our-metadata-model.md). Note that links to "internal" pages like this can only really be tested on PPE after the pages are built - GitLab isn't able to work out the right relative path.

Links to email someone are the same, just with the URL being "mailto:" followed by their email address. For example, emailing [Bob](mailto:bob@example.com)

## Images

Images are almost identical to links, just prefixed with an exclamation point and on their own line.

![DPI menu](images/how-to-publish/dpi-menu1.png)

## Tables

Tables are a little tricky, so it's best to look at the raw markdown to see how this is made:

| **Column 1** | **Column 2** |
| ----- | ----- |
| Row 1, Column 1 | Row 1, Column 2 |
| Row 2, Column 1 | Row 2, Column 2 |
| Row 3, Column 1 | Row 3, Column 2 |

## Lists

Hyphens are for un-ordered lists, with indented lists needing 2 spaces at the start of the row.

- bananas
- milk
    - strawberry milk
    - chocolate milk
- pasta

And numbers with a full stop (either correctly ordered, or just "1." repeated) are for numbered lists. Note that indenting needs 4 spaces, not two.

1. First
1. Second
    1. First part of the second
    1. Second part of the second
1. Third

## Code

There are a couple of pages with code blocks. These use three angled quotes (`) followed by the language of the code (eg, xml or json).

```xml
<xml>
    <xml-child>Hello</xml-child>
</xml>
```

```json
{
    "hello": "world"
}
```

## HTML

Markdown generates HTML, so if there is any HTML directly in the markdown, it will be included in the final HTML. For example, you can put a <br />line break in a specific point of a  paragraph. There are some tables like this.

Otherwise, a line gap in a paragraph looks like this.

## Information 'bubble'

The information bubble on the homepage is three exclamation points, a space, and the "information". The next line is indented by four spaces and the content there is displayed in the bubble.

!!! information
    Here is an information bubble. GitLab has no idea what it is though.
