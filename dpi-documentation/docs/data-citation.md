Every day millions of people use data. They search, publish, reuse and analyse. But very often, a major difficulty for the users is finding the right data. Citing data can help to mitigate this.

In this section you will find information about our initiatives on data citation.

## Data citation: A guide to best practice

[Data citation: A guide to best practice](https://doi.org/10.2830/59387) is a study and collection of best practices and recommendations in the field of data citation. It was published by the Publications Office of the European Union (OP) in 2022. Below you will find the annexes that contain useful quick reference materials including checklists, diagrams and examples. If you are interested in this topic, you can find a dedicated [data citation training](https://data.europa.eu/en/academy/data-citation-guide-best-practice-data-citation-citation-interoperability) in the data.europa academy where the author, Paul Jessop, presents the study in an online seminar.

### Checklist (long) for footnotes or reference-list entries

The following steps should be a checklist when creating a citation that will appear in a footnote or reference-list entry.

The key initial step is to refer to the metadata of the dataset that has been used. This may include a ‘cite this dataset as’ function. This will provide most, or all of the information needed, though it may appear in a slightly different format and some editing work may be needed.

Otherwise, the metadata should include all the information needed, though sometimes a little research will be needed to locate some of it.

| Metadata | Description |
| ----- | ----- |
| author(s) | Who does the cited dataset note as its creator or as a contributor to it? Is there a corporate author as well as the individual authors? How many are there and in what order do they appear in the metadata of the original dataset? Decide how many to include if there are more than three – this guide recommends a maximum of three (followed by ‘et al.’) but allows the inclusion of fewer or more in some circumstances.<br />Check whether the authors have been presented with their family name first (especially important with names from cultures that are not familiar to you). |
| ‘title’ or title (short code) | What is the accurate title (including capitalisation and punctuation) of the original dataset? Is it necessary to capitalise the title to meet ISG requirements (when the title is presented in italics)? Is there a short code by which the dataset is known, which can be placed in parentheses after the title? |
| version | What is the version of the dataset that was used in the activity for which it is being cited? Do not just pick up the most recent version unless this is what was used and is appropriate in the citation. |
| publisher | Who made the dataset available? They might already be included as an author, in which case leave them out here. Do not just assume that a repository is the publisher – there may be an actual publisher in the metadata. |
| date | When does the metadata say the data was published? For datasets that change, the date of initial publication may not be useful. However, sometimes it helps to distinguish similar but slightly different datasets. Is that the case here? Write the date (and time if needed) in datatypes indicated by [DCAT-AP](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe) (e.g., 2021-09-13 or 2021-09-13T23:16) or in the [Interinstitutional Style Guide](http://publications.europa.eu/code/en/en-000100.htm) format as appropriate. |
| date of citation ‘accessed’ | If the data may have changed since it was accessed, or may change in the future, at least note the date it was accessed. |
| PID | There should be a persistent identifier in the dataset metadata. This will be managed so that people can still use it to find and access the data in the future. Try to locate it and, if possible, write it as a web link so that it can be clicked on to access a landing page. Check that the web link works and gives the expected result before including it in a citation – a link that is already broken is of little use to readers. |

*Source:* Publications Office, Jessop, P., *Data citation: A guide to best practice*, 2022, [https://data.europa.eu/doi/10.2830/59387](https://data.europa.eu/doi/10.2830/59387)

### Checklist (short) for footnotes or reference-list entries

![Checklist](./images/data-citation/checklist.png)
 
*Source:* Publications Office, Jessop, P., *Data citation: A guide to best practice*, 2022, [https://data.europa.eu/doi/10.2830/59387](https://data.europa.eu/doi/10.2830/59387)

### Diagram for footnote and reference citation style with date format compliant with DCAT-AP

![Citation style for DCAT-AP](./images/data-citation/citation-dcat-ap.png)
 
*Source:* Publications Office, Jessop, P., *Data citation: A guide to best practice*, 2022, [https://data.europa.eu/doi/10.2830/59387](https://data.europa.eu/doi/10.2830/59387)

### Diagram for footnote and reference citation style with date format compliant with the Interinstitutional Style Guide

![Citation style for interinstitutional](./images/data-citation/citation-ii.png)
 
*Source:* Publications Office, Jessop, P., *Data citation: A guide to best practice*, 2022, [https://data.europa.eu/doi/10.2830/59387](https://data.europa.eu/doi/10.2830/59387)

### Examples of citations with date format compliant with DCAT-AP

European Commission, Directorate-General for Financial Stability, Financial Services and Capital Markets Union, ‘Consolidated list of persons, groups and entities subject to EU financial sanctions’, 2016 (updated 2020-12-23), accessed 2023-12-22, [http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions](http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions)

European Commission, Directorate-General for Internal Market, Industry, Entrepreneurship and SMEs, ‘Advanced Technologies for Industry - ATI’, 2020 (updated 2021-01-31), [http://data.europa.eu/88u/dataset/advanced-technologies-for-industry-ati](http://data.europa.eu/88u/dataset/advanced-technologies-for-industry-ati)

Sevini, F. and Arnés-Novau, X., ‘Export Control Handbook for Chemicals,’ version 2021 edition, European Commission, Joint Research Centre (JRC), 2021, accessed 2021-10-01, [http://data.europa.eu/89h/8f0d4fa2-110d-4c05-b2a8-ecd3addbf16b](http://data.europa.eu/89h/8f0d4fa2-110d-4c05-b2a8-ecd3addbf16b)

Nijs, W. and Ruiz, P., ‘01_JRC-EU-TIMES Full model’, European Commission, Joint Research Centre, 2019, [http://data.europa.eu/89h/8141a398-41a8-42fa-81a4-5b825a51761b](http://data.europa.eu/89h/8141a398-41a8-42fa-81a4-5b825a51761b)
European Commission, Eurostat, ‘Airport traffic data by reporting airport and airlines’ (avia_tf_apal), most recent data 2021-09-01, [https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en)

European Commission, Eurostat, ‘Real GDP growth rate – volume’ (tec00115), updated 2021-09-28, [https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en)
Publications Office of the European Union, ‘Country Named Authority List,’ 2009 (updated 2021-09-29), [http://data.europa.eu/88u/dataset/country](http://data.europa.eu/88u/dataset/country)

*Source:* Publications Office, Jessop, P., *Data citation: A guide to best practice*, 2022, [https://data.europa.eu/doi/10.2830/59387](https://data.europa.eu/doi/10.2830/59387)

### Examples of citations with date format compliant with the Interinstitutional Style Guide

European Commission, Directorate-General for Financial Stability, Financial Services and Capital Markets Union, ‘Consolidated list of persons, groups, and entities subject to EU financial sanctions,’ 2016 (updated 23 October 2020), accessed 1 October 2021, [http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions-fisma](http://data.europa.eu/88u/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions-fisma)

European Commission, Directorate-General for Internal Market, Industry, Entrepreneurship and SMEs, ‘Tenders Electronic Daily (TED) (csv subset) – public procurement notices,’ version 3.3, 2015 (updated 22 November 2020), [http://data.europa.eu/88u/dataset/ted-csv](http://data.europa.eu/88u/dataset/ted-csv)

Sevini, F. and Arnés-Novau, X., ‘Export control handbook for chemicals,’ version 2021 edition, European Commission, Joint Research Centre (JRC), 2021, accessed 1 October 2021, [http://data.europa.eu/89h/8f0d4fa2-110d-4c05-b2a8-ecd3addbf16b](http://data.europa.eu/89h/8f0d4fa2-110d-4c05-b2a8-ecd3addbf16b)

Nijs, W. and Ruiz, P., ‘01_JRC-EU-TIMES full model’, European Commission, Joint Research Centre, 2019, [http://data.europa.eu/89h/8141a398-41a8-42fa-81a4-5b825a51761b](http://data.europa.eu/89h/8141a398-41a8-42fa-81a4-5b825a51761b)

European Commission, Eurostat, ‘Airport traffic data by reporting airport and airlines’ (avia_tf_apal), most recent data 1 September 2021, [https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/avia_tf_apal/default/table?lang=en)

European Commission, Eurostat, ‘Real GDP growth rate – volume’ (tec00115), updated 18 September 2021, [https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en](https://ec.europa.eu/eurostat/databrowser/view/tec00115/default/table?lang=en)

Publications Office of the European Union, ‘Country named authority list,’ 2009 (updated 29 September 2021),[ http://data.europa.eu/88u/dataset/country]( http://data.europa.eu/88u/dataset/country)

*Source:* Publications Office, Jessop, P., *Data citation: A guide to best practice*, 2022, [https://data.europa.eu/doi/10.2830/59387](https://data.europa.eu/doi/10.2830/59387)

## Citation button

The citation button will help you retrieve citations for datasets without much effort and will guarantee their correctness.

**Where can you find it?**

View the web page of the dataset you want to cite. You will find the button on the dataset menu on the right above the dataset title.
 
![Citation button](./images/data-citation/citation-button.png)

**How can you use it?**

Click on ‘Cite’ and choose your preferred citation style. You can choose among the following: [EU Data Citation](https://data.europa.eu/doi/10.2830/59387), APA, Harvard, Vancouver.

![Citation button, expanded](./images/data-citation/citation-button-expanded.png)

Once you click on your preferred style, the citation will display on the screen.

![Citation button, copy](./images/data-citation/citation-button-copy.png)

Click the ‘Copy to clipboard’ and you can then paste the citation wherever you need.

