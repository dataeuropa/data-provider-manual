# Access Control and Tokens

The data section of data.europa.eu uses a middleware for access control. The middleware handles EU-Login and service accounts. Hence, every interaction with a restricted API endpoint requires an interaction with the middleware to obtain an access token (Party Token).


## Service accounts

If you have a service account associated with a catalogue, please see the example below to get an access token. You need to replace CLIENT_ID and CLIENT_SECRET with your credentials.


```
curl --location 'https://data.europa.eu/auth/middleware/login/service' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data '{
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET"

}'
```

This will return a response with the format


```
{
  "access_token" : "ACCESS_TOKEN..."
}
```

You can use this token to access the APIs of Hub-Repo, and Hub-Store to manage your data.


&nbsp;
&nbsp;

<!-- ## EU-Login accounts



Prerequisites

- You require valid credentials (username and password) for the Keycloak.
- There is no self-registration. Please contact the Publications Office of the European Union [data.europa.eu team](https://data.europa.eu/en/feedback/form) for further information how to require credentials.
- You need a tool to interact with a HTTP API, such as [Postman](https://www.postman.com/downloads/postman-agent/) or [curl](https://curl.se/) lfor the command line.

**Step 1 - Request User Token**

First you will need to require a User Token by performing a x-www-form-urlencoded POST request to the following endpoint:

[https://data.europa.eu/auth/realms/DEU/protocol/openid-connect/token](https://data.europa.eu/auth/realms/DEU/protocol/openid-connect/token)

The following form values need to be set:

| Key | Value |
| ----- | ----- |
| username | [yourusername] |
| password | [yourpassword] |
| grant_type | password |
| client_id | piveau-hub-ui |

Example with curl:

```
$ curl --location --request POST "https://data.europa.eu/auth/realms/DEU/protocol/openid-connect/token" \
--header "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "grant_type=password" \
--data-urlencode "cliend_id=piveau-hub-ui" \
--data-urlencode "username=[yourusername]" \
--data-urlencode "password=[yourpassword]"
```

If successful, you will receive a JSON response like this:

```json
{
    "access_token": "[yourusertoken]",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "[yourrefreshtoken]",
    "token_type": "Bearer",
    "not-before-policy": 0,
    "session_state": "694350c7-38b9-4051-bc2e-e15e34320133",
    "scope": "email profile"
}
```

**Step 2 Request Party Token**

Now you will need to require a Party Token by again performing a x-www-form-urlencoded POST request to the following endpoint:

[https://data.europa.eu/auth/realms/DEU/protocol/openid-connect/token](https://data.europa.eu/auth/realms/DEU/protocol/openid-connect/token)

The following form values need to be set:

| Key | Value |
| ----- | ----- |
| grant_type | urn:ietf:params:oauth:grant-type:uma-ticket |
| audience | piveau-hub-repo |

In addition, place the User Token from Step 1 into the header field Authorization with the leading string Bearer.

Example with curl:

```
$ curl --location --request POST "https://data.europa.eu/auth/realms/DEU/protocol/openid-connect/token" \
--header "Content-Type: application/x-www-form-urlencoded" \
--header "Authorization: Bearer [yourusertoken]" \
--data-urlencode "grant_type=urn:ietf:params:oauth:grant-type:uma-ticket " \
--data-urlencode "audience=piveau-hub-repo "
```

If successful, you will get a JSON response like this:

```json
{
    "upgraded": false,
    "access_token": "[yourpartytoken]",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "[yourrefreshtoken]",
    "token_type": "Bearer",
    "not-before-policy": 0
}
```

[yourpartytoken] can now be used to manage datasets in data.europa.eu.

**Token expiry**

For security reasons the tokens expire quickly. You can refresh them by just performing Step 1 and 2 again. -->
