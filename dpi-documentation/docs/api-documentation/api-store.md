# Managing files in the store


To manage files in the store you will need a valid JWT Bearer token. Please see the section Access control to learn how to get an access token.

The API of the Store is described [here](https://data.europa.eu/api/hub/store/).

The following commands will allow you to manage file storage in the data.europa infrastructure. Bear in mind that you need to replace CATALOGUE_ID and SECRET_TOKEN.


**Create File**


```
curl --location --request POST 'https://data.europa.eu/api/hub/store/data?catalog=CATALOGUE_ID' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Authorization: Bearer SECRET_TOKEN' \
--form 'test_file=@"/home/local_user/test.csv"'

```

**Update file**


```
curl --location --request PUT 'https://data.europa.eu/api/hub/store/data/66f3e4bd2bb9cd536b0fb5d5?catalog=CATALOGUE_ID' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Authorization: Bearer SECRET_TOKEN' \
--form 'test_file=@"/home/local_user/test_update.csv"'

```

**Delete file**


```
curl --location --request DELETE 'https://data.europa.eu/api/hub/store/data/66f3e4bd2bb9cd536b0fb5d5?catalog=CATALOGUE_ID' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Authorization: Bearer SECRET_TOKEN'

```

&nbsp;
&nbsp;
