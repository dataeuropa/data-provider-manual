Half of the human brain is dedicated to processing things visually. Therefore, visualising information helps people perceive and make sense of concepts and relationships in a complex modern world today. As a result, effective visualisations can contribute to a significantly better understanding of data and policies. They can achieve a considerably higher impact on policymakers and citizens and contribute to considerably higher political support.

The OP offers various services related to this competence, in the portal data.europa.eu, organisation of conferences, webinars, training material, etc.

## Visualisation tool

The **visualisation tool** uses as data source the files as provided by the source. By doing so, it allows users to grasp the data quickly and to understand if the dataset provides the information that the user is looking for. The ‘Preview’ button is available for all datasets in CSV format (more formats will be added over time). It can show the data in tabular and graphical format without the need for downloading.

Potential errors: it is possible that the tool may not accept the provided file format or that the files may be corrupted at the source. The portal has no influence on the source files.

## Geo-Visualisation

In addition to these functionalities for handling common types of geospatial data, the Geo- Visualisation component is also capable of previewing selected types of real-time data sources. Currently, certain themes provided via FIWARE Context Broker instances are supported. As shown below, the preview of real-time data can be opened in a similar manner as for the other types of geospatial data.

![Distributions](./images/data-visualisation/distributions.png)

The screenshot below shows an example of live tracking data of public buses in the city of Malaga. After opening the preview of such a data source, the view will be refreshed in regular time intervals (60 seconds). If the user clicks on one of the features displayed on the map, more detailed information is shown (in this case different properties of the tracked bus).

![Tracking buses](./images/data-visualisation/tracking-buses.png)

Potential errors: Sometimes a URL to a data source (WMS, GeoJSON file, or real-time data source) is broken or cannot be accessed temporarily. In these cases, the problem needs to be solved by the data provider.

For a better understanding you can watch the [introduction to geospatial data](https://youtu.be/gRLOgQ5Bibw) webinar, other [geospatial topics](https://data.europa.eu/de/geospatial-data-and-api-delivery) at the data.europa academy, and a [webinar on real time data](https://data.europa.eu/en/real-time-data).

## Further information

**EU DataViz** is an international conference on open data and data visualisation addressing the needs of the public sector across the EU. It is organised by the OP and the [first edition](https://op.europa.eu/en/web/eudataviz2019) was in 2019, followed by the [second edition](https://op.europa.eu/en/web/eudataviz) in 2021 as part of the [EU Open Data Days](https://op.europa.eu/en/web/euopendatadays/).

In a nutshell, the conference addressed how to translate complex issues and knowledge into digestible and relevant language that resonates with EU citizens and policymakers. Additionally, it helps to explore and analyse data to develop better policies.

Featuring expert speakers, EU DataViz 2021 provided an overview of innovative techniques and best practices used in both the private and the public sectors, offering the participants valuable insights into open-data and data-visualisation techniques and practices. The recordings, programme and presentations are available [on the website](https://op.europa.eu/en/web/eudataviz/programme).

Watch the [EU Open Data Days](https://www.youtube.com/watch?v=_PGRfvsNooQ) video and discover the EU DataViz event.

The data.europa academy contains a section specifically on [data visualisation](https://data.europa.eu/en/academy/data-visualisation) offering deeper understanding on this topic, its potential benefits and the necessary techniques to visualise open data.
