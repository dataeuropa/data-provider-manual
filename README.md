# Documentation of data.europa.eu (DEU)

This is the repository for the Documentation of data.europa.eu (DEU).

- Made with MkDocs - https://www.mkdocs.org/
- User Guide: https://www.mkdocs.org/user-guide/writing-your-docs/
- It is available here: https://dataeuropa.gitlab.io/data-provider-manual/ 

## Requirements
- Python 3 (tested with 3.8)
- Pip 3
- A Virtual Environment is recommended 

## Installation
- Clone the repository and navigate into the directory
- We are using weasyprint, please read this for installation instructions:  https://doc.courtbouillon.org/weasyprint/latest/first_steps.html#installation
****
```
$ pip install -r requirements.txt
```
- Build the documentation
```
$ mkdocs build -f dpi-documentation/mkdocs.yml -d ../public
```
- Develop and preview with Live Reload
```
$ cd dpi-documentation
$ mkdocs serve
```
- Browse to http://localhost:8000

